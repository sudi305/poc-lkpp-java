/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100011
 Source Host           : localhost:5433
 Source Catalog        : poc-2020
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100011
 File Encoding         : 65001

 Date: 31/01/2020 00:19:24
*/


-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "users";
CREATE TABLE "users" (
  "id" int8 NOT NULL DEFAULT NULL,
  "nama" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "password" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "email" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "username" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "role_id" int8 DEFAULT NULL,
  "npwp" varchar COLLATE "pg_catalog"."default" DEFAULT NULL
)
;
ALTER TABLE "users" OWNER TO "postgres";

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "users" VALUES (1, 'Admin', 'admin-1234', 'admin@adm.com', 'admin', 2, NULL);
INSERT INTO "users" VALUES (2, 'Pembeli', 'pembeli-1234', 'pembeli@gmail.com', 'pembeli', 3, '11.111.111.1-111.111');
INSERT INTO "users" VALUES (3, 'Penjual', 'penjual-1234', 'penjual@gmail.com', 'penjual', 4, '11.111.111.1-111.111');
COMMIT;

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "u_username" UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");

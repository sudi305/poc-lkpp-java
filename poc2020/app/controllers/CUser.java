package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import com.google.gson.Gson;
import java.util.stream.Collectors;

import controllers.BaseController.LoginInfo;
import models.User;
import models.Role;

public class CUser extends BaseController {

    public static void index() {
        List<User> users = User.getAll();
        String transJson1 = new Gson().toJson(users);
        render(users, transJson1);
    }

    public static void form(Long id){
        User user = new User();

        if(id != null) user = User.findById(id);
        List<Role> roles = Role.getAll();

        render(user, roles);
    }

    public static void register(Long id) {
        User user = new User();
        if(id != null) user = User.findById(id);
        List<Role> roles = Role.getAll();
        roles.removeIf(f->"Admin".equals(f.name));
        render(user, roles);
    }

    public static void delete(Long id){
        User user = User.findById(id);
        user.delete();
        redirect("/CUser/index");
    }

    public void formSubmit(User user){
        user.save();
        LoginInfo login = loginInfo.get();
        if(login.role_id == 2){
            redirect("/CUser/index");
        }else{
            redirect("/Application/index");
        }
    }

    public void formRegister(User user){
        user.save();
        LoginInfo login = loginInfo.get();
            redirect("/Application/index");
    }

    public static void profile(Long id){
        User user = User.findById(id);
        render(user);
    }

}
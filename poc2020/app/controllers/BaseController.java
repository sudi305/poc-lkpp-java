package controllers;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;

import controllers.BaseController.LoginInfo;
import models.User;
//import models.jcommon.db.base.BaseModel;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.mvc.After;
import play.mvc.Before;
import play.mvc.Controller;

public class BaseController extends Controller {

	/**Menyimpan data yg ada session
	 * 
	 * @author Andik Yulianto (andik@lkpp.go.id)
	 *
	 */
	public static class LoginInfo implements Serializable
	{
        public Long id;
        public String email;
		public String username;
		public String nama;
        public Long role_id;
        
		public String toString()
		{
			return String.format("id: %d, username: %s, email: %s", 
					this.id, username, email);
        }
        
		public User getUser()
		{
			if(id==null)
				return null;
			else
				return User.findById(id);
		}
	}
	
	/*
	 */
	public static ThreadLocal<LoginInfo> loginInfo=new ThreadLocal<>();


	@Before(priority=0)
	protected static void prepareData()
	{
		//Ambil data dari sessoin dan taruh di ThreadLocal
		String str=session.get("login_info");
		LoginInfo data;
		if(str==null)
			data=new LoginInfo();
		else
		{
			data=new Gson().fromJson(str, LoginInfo.class);
			session.put("SESSION_USERID", data.username);
		}
		loginInfo.set(data);
		renderArgs.put("loginInfo", data);

		String buildInfo=Cache.get("buldInfo", String.class);
		if(buildInfo==null)
		{
			try {
				buildInfo=FileUtils.readFileToString(new File(Play.applicationPath + "/precompiled/build"), "UTF-8");
				Cache.set(buildInfo, "buildInfo");
			} catch (IOException e) {
			}
		}
		renderArgs.put("buildInfo", buildInfo);
	}
	
	@play.mvc.Finally(priority=99)
	protected static void saveToSession()
	{
		//simpan loginInfo ke session
		LoginInfo data=loginInfo.get();
		if(data!=null)
		{
			String str=new Gson().toJson(data);
			session.put("login_info", str);
		}
		else
			session.remove("login_info");
	}
}

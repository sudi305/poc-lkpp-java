package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import com.google.gson.Gson;

import models.User;

public class CAuth extends BaseController {

    public static void login() {
        render();
    }

    public static void loginPost(String username, String password){
        
        User user = User.findByUserNameAndPwd(username, password);
        //System.out.println("User : " + new Gson().toJson(user));
        if(user != null){
            LoginInfo login=loginInfo.get();
            login.id = user.id;
            login.username = user.username;
            login.email = user.email;
            login.nama = user.nama;
            login.role_id = user.role_id;

            BaseController.saveToSession(); //simpan ke loginInfo
            BaseController.prepareData();//
            
            redirect("Application.index");
        }
        else{
            flash.error("User Tidak ditemukan untuk Username atau Password yang di input");
            render("/CAuth/login.html");
        }
    }

    public static void logout(){
        loginInfo.remove();
        session.clear();
        redirect("Application.index");
    }

    public static void forgotPassword() {
        render();
    }
    
    public static void forgotPasswordSubmit(User user) {
        User realUser = User.findByUserNameAndEmail(user.username, user.email);
        if(realUser!=null) {
        String newPassword = generateNewPassword();
            realUser.password = newPassword;
            realUser.save();
            render(newPassword);
        }
    }

    static String generateNewPassword() {
        String alphaseeds="qwertyuiopasdfghjklzxcvbnm1234567890";
        String specialChars="!@#$%^&*(),./?><'[]|=-_+";
        String newPassword="";
        int alphaseedLength=alphaseeds.length();
        int specialCharLength=specialChars.length();
        int passwordLength = 10;
        
        Random r = new Random();
        int specialCharPos = r.nextInt(passwordLength);
        for(int i=0;i<passwordLength;i++) {
            if(i==specialCharPos)
                 newPassword+=specialChars.charAt(r.nextInt(specialCharLength));
            newPassword+=alphaseeds.charAt(r.nextInt(alphaseedLength));
        }
        return newPassword;
    }

}
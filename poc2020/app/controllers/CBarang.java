package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import com.google.gson.Gson;

import models.Barang;

public class CBarang extends BaseController {

    public static void index() {
        LoginInfo login = loginInfo.get();
        List<Barang> barangs = new ArrayList<>();
        if(login.role_id == 2){
            barangs = Barang.getAll();
        }else{
            barangs = Barang.getByUserId(login.id);
        }
        String transJson3 = new Gson().toJson(barangs);
        render(barangs, transJson3);
    }

    public static void form(Long id){
        Barang barang = new Barang();
        if(id != null) barang = Barang.findById(id);
        System.out.println("barang"+barang.id);
        render(barang);
    }

    public static void delete(Long id){
        Barang barang = Barang.findById(id);
        barang.delete();
        redirect("/CBarang/index");
    }

    public void formSubmit(Barang barang, Float price){
        if(barang.id == null){
            LoginInfo login = loginInfo.get();
            barang.user_id = login.id;
        }
        barang.harga = price;
        System.out.println("barang price " + price);
        System.out.println("barang nama " + barang.nama);
        System.out.println("barang harga " + barang.harga);
        System.out.println("barang stok " + barang.stok);
        barang.save();
        redirect("/CBarang/index");
    }

}
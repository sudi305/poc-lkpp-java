package models;

import play.data.validation.Required;
import java.util.List;

//import play.db.jdbc.BaseTable;
//import play.db.jdbc.Id;
//import play.db.jdbc.Table;

import play.db.jpa.*;
import javax.persistence.*;

@Entity
@Table(name="users")
public class User extends Model {

    //@Id(function = "nextval", sequence = "seq_users")
	//public Long id;
    
    @Required
    public String username;
    
    @Required
    public String password;
    
    @Required
    public String nama;
    
    @Required
    public String email;
    
    @Required
    public String npwp;
    
    
    public Long role_id;

    public static List<User> getAll()
	{
		return find("NOT nama LIKE '?%' ORDER BY id desc").fetch();
    }

    public Role getRole(){
        if(role_id==null)
			return null;
		return Role.findById(role_id);
    }

    public static User findByUserNameAndPwd(String username, String password) {
        return find("byUsernameAndPassword", username, password).first();
	}
    public static User findByUserNameAndEmail(String username, String email) {
        return find("byUsernameAndEmail", username, email).first();
	}
}
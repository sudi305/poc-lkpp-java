//convert ke array buffer
function s2ab(s) {
    if(typeof ArrayBuffer !== 'undefined') {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    } else {
        var buf = new Array(s.length);
        for (var i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }
}
//create-workbook
function generateXlsxBarang() {

    var ws_name = "BARANG";

    var wscols = [
        {wch:4},
        {wch:15},
        {wch:15},
        {wch:15},
        {wch:15},
        {wch:15}
    ];

    var wsrows = [
        {hpt: 17},
        {hpt: 47},
    ];

    var ws_data = [["Report User"],["No","Nama", "Deskripsi", "Harga","Stok","User"]];

    for(var i = 0; i < data_barang.length; i++){
        var model = [ i + 1, 
            // data_barang[i]['id'], 
            data_barang[i]['nama'], 
            data_barang[i]['deskripsi'], 
            data_barang[i]['harga'], 
            data_barang[i]['stok'],  
            data_barang[i]['user_id']];
        ws_data.push(model);
    }
    //     public Long id;
    // public String nama;
    // public String deskripsi;
    // public Float harga;
    // public Integer stok;
    // public Long user_id;

    var wb = XLSX.utils.book_new();

    var ws = XLSX.utils.aoa_to_sheet(ws_data);

    ws['!merges'] = [{ s: 'A1', e: 'E1' }];

    ws['!cols'] = wscols;

    //ws['!rows'] = wsrows;

    //ws['!autofilter'] = { ref: "A4:E4" };

    XLSX.utils.book_append_sheet(wb, ws, ws_name);

    var wopts = { bookType:'xlsx', bookSST:false, type:'binary' };

    var wbout = XLSX.write(wb,wopts);

    var filename = "Barang.xlsx";

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
}

function generateJsonBarang(){
    saveAs(new Blob([JSON.stringify(data_barang)],{type:"application/json"}), "Barang.json");
}

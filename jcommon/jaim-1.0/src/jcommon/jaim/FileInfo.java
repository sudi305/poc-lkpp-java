package jcommon.jaim;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileInfo
{
	public FileInfo(String name, String modifiedDate, String size) {
		this.size = Long.parseLong(size.trim());
		try {
			this.modifiedDate = new SimpleDateFormat("dd-MMM-yyyy hh:mm").parse(modifiedDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.name = name.replace('\\', '/'); //convert as '/'
	}
	public long size;
	public Date modifiedDate;
	public String name;
	
	public int hashCode()
	{
		return name.hashCode();
	}
	
	/**
	 * equals if name & hash equal
	 */
	public boolean equals(Object o)
	{
		FileInfo i=(FileInfo)o;
		boolean eq=i.name.equals(name) && i.modifiedDate.equals(modifiedDate);
		return eq;
	}
	
	public String toString()
	{
		return String.format("%s, hash: %s, size: %,d", name, modifiedDate, size);
	}
	
}
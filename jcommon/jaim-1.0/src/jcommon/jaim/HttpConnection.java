package jcommon.jaim;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;

/**KOneksi ke HTTPS Server dengan Certificate dan URL yg telah ditetapkan.
 * URL -> jaim-server.lkpp.go.id
 * @author Mr. Andik
 *
 */
public class HttpConnection {
	
	
	private  CloseableHttpClient httpclient;
	
	public HttpConnection(String user, String password) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException
	{
		
	    org.apache.http.ssl.SSLContextBuilder context_b = SSLContextBuilder.create();
        context_b.loadTrustMaterial(new TrustStrategy() {
			@Override
			public boolean isTrusted(X509Certificate[] cert, String arg1) throws CertificateException {
				String signature=Hex.encodeHexString(cert[0].getSignature());
				return ("82b4fa676c4320f8936bb010030f0e037d345e51bff44d6af857dd3ac9bf8affd35d3863bf8b932ba68baad0db4f24295842c2c1ccb7f6c106a46d89faca176a3960a95edf9f45a18e7a28eba8ac6ffc4c24a134f4a5e5aec78d6f10de716e3be72576df071b4ab2cf07f38aac51815555a5a7c659bebf95686c2704624540aa3e38ea5eadee0d28e6538ed3c292eba3247fdb7a00ad973a37ec6bfb2387115d32772c8e1c937e9b508dd1f9327fbc6902cf8031b96e96b06e9b00d43d79fd6514a4562835afa22aa6d9a01d5868a6fa43536f3fe7d806c8bb19552310fa3e2c4115fa3ef87aa1146833f4abca2b3cc520a4a70541566227a17ef53dd0018412").equals(signature);
			}
		});
        
        SSLContext ssl_context = context_b.build();
        org.apache.http.conn.ssl.SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(ssl_context,
              new HostnameVerifier() {
				
				@Override
				public boolean verify(String host, SSLSession arg1) {
					return "jaim-server.lkpp.go.id".equals(host);
				}
			} );

        HttpClientBuilder builder = HttpClients.custom()
            .setSSLSocketFactory(sslSocketFactory);
        
   
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope("jaim-server.lkpp.go.id", 443),
                new UsernamePasswordCredentials(user, password));
        
     	builder.setDefaultCredentialsProvider(credsProvider);
     	
     	
        httpclient = builder.build();
        
	}
	
	public InputStream get(String url) throws ClientProtocolException, IOException
	{
	      try {

	            HttpGet httpget = new HttpGet(url);

	            CloseableHttpResponse response = httpclient.execute(httpget);
	            try {
	                HttpEntity entity = response.getEntity();
	                StatusLine status=response.getStatusLine();
	                switch(status.getStatusCode())
	                {	case HttpStatus.SC_OK:
	                		return entity.getContent();
	                	case HttpStatus.SC_UNAUTHORIZED:
	                		throw new Exception("Error Invalid user/password.");
	                	default:
		                	throw new Exception("Error HTTP: " +  url + " : " + status.getReasonPhrase());
	                }	
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
	      return null;
	}
}

package jcommon.jaim;

import java.io.BufferedOutputStream;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

public class Jaim {

	/**
	 * password for keystore: k3yst0r3
	 */
	
	private static final Logger logger=Logger.getLogger(Jaim.class);
	private Properties metadata;
	
	private String application;
	private String targetPath;
	private String sourceUrl;
	private int totalC=0;
	private int totalN=0;
	private int totalU=0;
	private int totalS=0;
	private int totalD=0;//deleted
	
	private String rootUrl="";
	private HttpConnection conn;
	private String skipIfExists;
	private String[] skipFiles=new String[0];
	
	private List<FileInfo> files=new ArrayList<>();
	private List<String> fileNames=new ArrayList<>();
	private String mode;
	
	
	/**Metadata dari aplikasi yang akan di-load
	 * 
	 * @param jimMetadata
	 * @param password 
	 * @param user 
	 * @throws IOException
	 */
	public Jaim(String jimMetadata, String user, String password) throws IOException
	{
		metadata=new Properties();
		File file=new File(jimMetadata);
		if(!file.exists())
			throw new IOException("File not found in current folder: " + file.getPath());
		InputStream is=new FileInputStream(file);
 
		metadata.load(is);
		
		targetPath=metadata.getProperty("target.path");
		if(StringUtils.isEmpty(targetPath))
			targetPath=System.getProperty("user.dir");
		application=metadata.getProperty("application");
		sourceUrl=metadata.getProperty("source.url");
		mode=metadata.getProperty("mode");
		skipIfExists=metadata.getProperty("skip.if.exists");
		if(skipIfExists!=null)
			skipFiles=skipIfExists.split(";");
		
		info("Installer metadata -> '%s'", file.getPath());
		info("\tSource -> %s", sourceUrl);
		info("\tTarget -> %s/%s", targetPath, application);
		info("\tInstaller-Man -> %s | starts: %s", user, new Date());
		Console cons=System.console();
		if(cons!=null)
		{
			String confirm=cons.readLine("Are you sure to continue? [Y]/[N]");
			if(confirm==null)
				System.exit(-1);
			confirm.toLowerCase();
			if(!confirm.contains("y"))
				System.exit(-1);;
		}
		
		
		try {
			conn=new HttpConnection(user, password);
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | CertificateException e) {
			e.printStackTrace();
		}
	}
	
	/**Dipanggil via command line.
	 * System akan mencari file jaim.properties pada classpath
	 * 
	 * @param args[0] jaim_properties yg ada di Jaim Server
	 * @param args[1] user yg ada di Jaim Server
	 * @param args[2] password yg ada di Jaim Server
	 * @throws IOException
	 */
	public static void main(String[] args)  {
		System.out.format("Jaim (Java Install Manager) versi 1.0\n");
		if(args.length==0 || args.length>3)
		{
			System.out.println("Usage: java -jar jaim-1.0.jar jaim_properties user [password]");
			System.out.println("\tjaim_properties -> contains installation metadata");
			System.out.println("\tuser -> user of installator in LKPP server");
			System.out.println("\t[password] -> password (optional) of installator in LKPP server");
			return;
		}
		String password="";
		if(args.length==3)
			password=args[2];
		else
		{
			Console cons=System.console();
			char[] passChars=cons.readPassword("Password: ");
			password=new String(passChars);
		}
		
		Jaim jaim=null;
		try {
			jaim = new Jaim(args[0], args[1], password);
		} catch (IOException e) {
			error(e.toString());
			return;
		}
		//Mode prod, latihan, or none?
		if(StringUtils.isEmpty(jaim.mode))
			jaim.doInstall("");
		else
		{	
			String[] ary=jaim.mode.split(",");
			for(String st: ary)
				jaim.doInstall(st);
		}
	}
	
	/**Do install
	 * @param mode production| latihan
	 */
	private void doInstall(String mode) {
		
		totalC=0;
		totalN=0;
		totalU=0;
		totalS=0;
		totalD=0;//deleted
		//open URL
		rootUrl=String.format("%s/%s", sourceUrl, application);
		
		try {
			info("Download Metadata Information [%s] -> %s", mode, rootUrl);
			getMetadata("");
			
			
		} catch (MalformedURLException e) {
			error("Invalid URL (%s): %s", e, rootUrl);
			return;
		} catch (IOException e) {
			error("IOException (%s): %s", e, rootUrl);
			return;
		}
			
		info("Number of files: %4d",  files.size());
		info("[C]reate, [U]update, [N]o change, [S]kip, [D]elete");
		StopWatch sw=new StopWatch();
		sw.start();
		int i=0;
		FileInfo currentInfo=null;
		try
		{
			for(FileInfo info: files)
			{
				i++;
				currentInfo=info;
				processSingleFile(i, info, mode);
			}
		}
		catch(Exception e)
		{
			error("Error %s while accessing: %s", e, currentInfo);
		}
		
		//delete files if not exists in remote
		
		String rootDir=String.format("%s/%s/%s", targetPath, mode, application);
		int index=rootDir.length()-1;
		Collection<File> localFiles=FileUtils.listFiles(new File(rootDir), null, true);
		for(File file: localFiles)
		{
			String relFile=file.getPath().substring(index).replace('\\', '/');
			if(!fileNames.contains(relFile))
			{
				totalD++;
				info("Delete file: %s", file);
				file.delete();
			}
		}
		
		sw.stop();
		debug("DONE: %4d[C] %4d[U] %4d[N] %4d[S]  %4d[D], duration: %s\n\n", totalC, totalU, totalN, totalS, totalD, sw);
	}

	private void getMetadata(String path) throws ClientProtocolException, IOException {
		InputStream is=conn.get(rootUrl + path);
		String metadata=IOUtils.toString(is);
		is.close();
		Elements elements=Jsoup.parse(metadata).select("pre");
		if(elements.size()==1)
		{
			Pattern pattern=Pattern.compile("<a href=\"(.+)\">.*</a>\\s+(.+)\\s+([\\d\\-]+)"); //catch href=""
			String text=elements.get(0).html();
			String[] rows=text.split("\n");
			for(String row: rows)
			{
				row=row.trim();
				if(row.endsWith("../</a>"))
					continue;
				Matcher m= pattern.matcher(row);
				if(m.matches())
				{
					String dir=m.group(1);
					String date=m.group(2);
					String size=m.group(3);
					if(dir.endsWith("/"))
						getMetadata(path + "/" + dir.substring(0, dir.length()-1));
					else
					{
						FileInfo fileInfo=new FileInfo(path + "/" + dir, date, size);
						files.add(fileInfo);
						fileNames.add(fileInfo.name);
					}
				}
				else
					error("not match %s", row);
			
			}
		}
	}

	/**Download jika MD5-nya beda dengan file local
	 * @param i 
	 * 
	 * @param info
	 * @throws MalformedURLException 
	 */
	private void processSingleFile(int i, FileInfo info, String mode) throws MalformedURLException {
		

		//check local file
		String pathWithMode=targetPath + mode + "/" + application;
		File localFile=new File(FilenameUtils.separatorsToSystem(pathWithMode) + File.separator + FilenameUtils.separatorsToSystem(info.name));
		char status='N';
		
		if(!localFile.exists()) ///not exists
		{
			localFile.getParentFile().mkdirs();
			status='C';
			totalC++;
		}
		else //exist but have the same MD5?
		{
			try {
				//should be skipped or not?
				if(skipped(info.name))
				{
					status='S';
					totalS++;
				}
				else
				{

					if(localFile.lastModified()!=info.modifiedDate.getTime())
					{
						status='U';
						totalU++;
					}
					else
						totalN++;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
				
		if(status=='C' || status=='U')
		{
			//copy from remote
			String url=String.format("%s/%s/%s",this.sourceUrl, application,info.name);
			OutputStream os;
			try {
				os = new BufferedOutputStream(new FileOutputStream(localFile));
				InputStream is=conn.get(url);
				IOUtils.copy(is, os);
				os.close();
				is.close();
				localFile.setLastModified(info.modifiedDate.getTime());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		if(status!='N')
			info("Processing file# %4d [%s] %s/%s", i, status, application, info.name);
	}

	private boolean skipped(String name) {
		for(String wildcard: skipFiles)
			if(FilenameUtils.wildcardMatch(name, wildcard))
				return true;
		return false;
	}

	public  static void debug(String format, Object ... values)
	{
		logger.debug(String.format(format, values));
	}
	
	public static void info(String format, Object ... values)
	{
		logger.info(String.format(format, values));
	}
	
	public static void error(String format, Object ... values)
	{
		logger.error(String.format(format, values));
	}
	

}
  
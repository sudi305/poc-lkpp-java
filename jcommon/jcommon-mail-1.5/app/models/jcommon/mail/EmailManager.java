package models.jcommon.mail;

import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import jobs.MailSenderLauncherJob;
import models.jcommon.config.Configuration;
import models.jcommon.util.DateUtil;
import play.Logger;
import play.templates.Template;

/**Class ini digunakan untuk memasukkan email baru ke antrian 
 * */
public class EmailManager {
		
	/**Create a MailQueue. Mail is not send yet. Call save to enqueue the email
	 * @return MailQueue. 
	 * @throws Exception 
	public static MailQueue createEmail(String to_address, String templateName, Map variables) throws Exception  
	{
		String[] params=mergeTemplate(templateName, variables); 		
		MailQueue mq=createEmail(to_address, params[0], params[1]);
		return mq;
	}
	*/
	/**Create a MailQueue. Mail is not send yet. Call save to enqueue the email
	 * @return MailQueue. 
	 * @throws Exception */
	public static MailQueue createEmail(String to_address, Template template, Map<String, Object> variables) throws Exception  
	{ 		
		String body =template.render(variables);
//		Logger.info(body);
		MailQueue mq=createEmail(to_address, body, (String)variables.get("subject"));
		return mq;
	}

	
	/**Create a MailQueue. Mail is not send yet. Call save to enqueue the email
	 * @return MailQueue. */
	public static MailQueue createEmail(String to_address, String body, String subject) throws Exception 
	{	MailQueue mq=new MailQueue();
		mq.enqueue_date=DateUtil.newDate();		
		mq.body=Base64.encodeBase64String(Base64.encodeBase64String(body.getBytes()).getBytes());
		mq.subject=subject;	
		mq.to_addresses=to_address;
		mq.status=MailQueue.MAIL_STATUS.INBOX; //status awal
		mq.retry=0;
		mq.engine_version=MailQueue.ENGINE_VERSION;
		mq.mime=1;
		mq.lls_id=0l;//cannot be empty
		mq.from_address=Configuration.getConfigurationValue(MailQueue.SMTP_SENDER);
		if(mq.from_address==null)
			throw new NullPointerException("[CONFIGURATION] mail.sender belum diisi");	
		
		/**Berikan sinyal ke MailSenderLauncherJob agar segera create Job. Tujuannya supaya
		 * mail yang baru di-create ini segera terkirim tanpa menunggu jadwal pengiriman 
		 */
//		MailSenderLauncherJob.createJobIfNecessary();
		return mq;
	}
	
	
	
	/** Merge a velocity template 
	 * @param id
	 * @return array of String [body, subject]
	 * @throws Exception
	private static String[] mergeTemplate(String templateName, Map variables) throws Exception
	{
//		Velocity.setProperty("file.resource.loader.class", "models.jcommon.mail.VelocityDBResourceLoader");
		Velocity.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		Velocity.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		Velocity.init(); 
		
		//add variables to context
		VelocityContext ctx=new VelocityContext();		
		for(Object key:variables.keySet())
			ctx.put((String)key, variables.get(key));
				
		//render the template
		ByteArrayOutputStream os=new ByteArrayOutputStream(10000);
		Writer output=new OutputStreamWriter(os) ;
		Velocity.mergeTemplate(TEMPLATE_PATH+"/"+templateName,"UTF-8", ctx, output);
		String subject=(String)ctx.get("subject");
		if(subject==null)
			subject=(String) variables.get("subject");
		output.flush();		
		String result[]=new String[]{Base64.encodeBase64String(os.toByteArray()), subject};
		return result;
	}
	 */
	
	public static MailQueue KirimUlang(Long id) throws Exception {	
		MailQueue mq=MailQueue.findById(id);
		mq.enqueue_date=DateUtil.newDate();
		mq.status=MailQueue.MAIL_STATUS.INBOX; //status awal
		mq.retry=0;
		mq.engine_version=MailQueue.ENGINE_VERSION;
		mq.mime=1;
		mq.from_address=Configuration.getConfigurationValue(MailQueue.SMTP_SENDER);
		if(mq.from_address==null)
			throw new NullPointerException("[CONFIGURATION] mail.sender belum diisi");
		
		return mq;
	}
	
}

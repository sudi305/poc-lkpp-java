/**migrasi configurasi email. Nama CFG_CATEGORY mengikuti properties java Mail
*/
update configuration set cfg_sub_category='mail.delay' where cfg_sub_category='epns.mail-delay' and cfg_category='CONFIG';
update configuration set cfg_sub_category='mail.retry' where cfg_sub_category='epns.mail-retry' and cfg_category='CONFIG';
update configuration set cfg_sub_category='mail.smtp.host' where cfg_sub_category='epns.mail-server' and cfg_category='CONFIG';
update configuration set cfg_sub_category='mail.smtp.user' where cfg_sub_category='epns.mail-user' and cfg_category='CONFIG';
update configuration set cfg_sub_category='mail.sender' where cfg_sub_category='epns.mail-sender' and cfg_category='CONFIG';

package models.bm;

import java.text.ParseException;
import java.util.Date;

import javax.persistence.EnumType;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.DateUtil;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**BroadcastMessage (BM) adalah data yang dikirim dari pusat: CDX ke seluruh SPSE untuk ditampilkan di SPSE.
 * Lokasi menampilkan, durasi penampilan ditentukan oleh beberapa parameter.
 * 
 * Beberapa contoh fungsi BM
 * 1. Running teks (marqueue) di halaman publik
 * 2. Pesan khusus di halaman Panitia: misal "LKPP telah merilis Computer Based Training untuk SPSE 4".
 * 
 * Pesan ditampilkan berdasarkan ROLE. Untuk role PUBLIC, akan ditampillan di halaman utama.
 * 
 * @author andik
 *
 */
@Table(name="BROADCAST_MESSAGE")
public class BroadcastMessage extends BaseModel {
	@Enumerated(EnumType.STRING)
	public enum Category
	{
		RUNNING_TEXT,
		STATIC_LINK
	}
	
	/**ID di-generate oleh DAO karena Class BroadcastMessage ini digunakan oleh CDX (server) dan SPSE(client)
	 * SPSE tidak pernah meng-create BM karena SPSE hanya menerima dari CDX. 
	 * Oleh karena itu hanya CDX yang create new sekaligus generate id-nya 
	 */
	@Id
	public Long bm_id;
	
	public String title;
	
	/**
	 * Untuk tipe STATIC_LINK, content ini berisi abosolute URL yang dimaksud 
	 */
//	@Column(columnDefinition="text")
	public String content; //HTML yang akan di-print
	
	/**Field ini digunakan khusus untuk RUNNING_TEXT.
	 * yaitu berisi URL yang menuju halaman di LKPP
	 */
	public String url;
	
//	@Column(name="order_", nullable=false)
	public int order_;
	
	/** Category pesan: running text atau static link
	 * 
	 */
	public Category category;
	
	/**Pada halaman HOME role ini, pesan akan ditampilkan.
	 * Comma-separated dan selalu diawali & diakhiri tanda , (untuk mempermudah query): 
	 * 	- ,PUBLIC,
	 *  - ,PANITIA,PPK,ADMIN_AGENCY,
	 * PUBLIC: ditampilkan di halaman publik
	 * 
	 */
//	@Column(name="roles_", length=512,  nullable=false)
	public String roles_; 

	
	/**Jika BM ini STATIC_LINK, maka imageData ini berisi image menjadi
	 * link. Sedangkan content berisi URL 
	 * 
	 */
//	@Column(columnDefinition="text", name="image_data")
	public String image_data;
		
	/**Sampai kapan pesan ditampilkan?
	 * Null artinya forever
	 */
//	@Column(name="end_date", nullable=false)
	public Date end_date;
	
	/**Link to Blob_table
	 * 
	 */
	public Long id_attachment;
	
	public BlobTable getAttachment()
	{
		if(id_attachment==null)
			return null;
		else
			return BlobTableDao.getLastById(id_attachment);
	}
	
	
	public void prePersist()
	{
		super.prePersist();
		if (roles_ != null) {
			// pastikan roles dengan format yang benar; diawali dan diakhiri ','
			// tanpa spasi di antara role
			String ary[] = roles_.split(",");
			StringBuilder str = new StringBuilder(roles_.length() + 2);
			str.append(',');
			for (String role : ary) {
				role = role.trim();
				if (role.length() > 0)
					str.append(role).append(',');
			}
			roles_ = str.toString();
		}
		if (end_date == null) { // jika null maka set menjadi MAX-DATE 
			try {
				end_date = DateUtil.parseSmallDate("31-12-9999", "00:00");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

}

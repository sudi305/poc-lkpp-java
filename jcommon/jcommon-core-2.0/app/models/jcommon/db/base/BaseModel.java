package models.jcommon.db.base;

import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.MappedSuperclass;
import models.jcommon.util.DateUtil;
import play.cache.Cache;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.libs.Crypto;
import play.mvc.Scope.Session;

/**
 * BaseModel; specific for SPSE 4.x
 * @author andik
 *
 */
@MappedSuperclass
public class BaseModel extends BaseTable {
	
	public static final Date MAX_DATE = new GregorianCalendar(2999,12,31).getTime();
	
	public static final String SESSION_USERID = "userId";

	public String audittype;
	
	public String audituser;
	
	public Date auditupdate;

	/**
	 * Fungsi {@code baseModelPrePersist} dijalankan ketika sebuah model akan dibuat ataupun di-update.
	 * Fungsi ini melakukan setting terhadap kolom audittype, audituser dan auditupdate.
	 */
//	@PrePersist
//	@PreUpdate
	protected void prePersist() {
		audittype = audittype != null && !audittype.isEmpty() ? "U" : "C";
		if (audituser == null || audituser.isEmpty()) {
			Session current_session = Session.current();
			if (current_session != null) {
				audituser = current_session.get(SESSION_USERID); // set audituser
				if (audituser == null)
					audituser = "N/A";
			} else { // handling jika session object tidak ada, misal saat mengerjakan JOB
				audituser = "_UNKNOWN_";
			}
		}
		auditupdate = DateUtil.newDate();
	}
	
	/**Dapatkan nextValue dari sequence tertentu */
	public static Long getNextSequenceValue(String sequenceName) throws SQLException
	{
		return Query.find("select nextval(?)", Long.class, sequenceName).first();
	}
	
	public static String encrypt(String str)
	{
		return Crypto.encryptAES(str, "MNJK8*92lkpojdas");
	}
	
	public static String decrypt(String str)
	{
		return Crypto.decryptAES(str, "MNJK8*92lkpojdas");
	}
	
	public static String getTableName(Class<? extends BaseModel> className)
	{
		Table table=className.getAnnotation(Table.class);
		if(table==null)
			return null;
		else
		{
			String schema=table.schema();
			if(schema.length()!=0)
				schema=schema+".";
			String tableName=table.name();
			return schema+tableName;
		}
	}
	
	public String getTableName()
	{
		Class className=this.getClass();
		return getTableName(className);
	}

}

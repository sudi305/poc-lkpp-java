package models.jcommon.util;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.joda.time.DateTime;

public class DateUtil {

	private static Map<Integer, Long> dateMap = new ConcurrentHashMap<Integer, Long>(20);
	
//	public static String SESI_DEFAULT="DEFAULT";
	
	private static String DATE_FORMAT_SMALL="dd-MM-yyyy";
	private static String TIME_FORMAT="HH:mm";
	private static String DATE_TIME_FORMAT_SMALL=DATE_FORMAT_SMALL + " " + TIME_FORMAT;

    // mendapatkan informasi waktu
    // http://stackoverflow.com/questions/24937618/memory-consumption-java-util-date-vs-java-time-localdate-vs-org-joda-time-dateti
    public static long getTime() {
        return System.currentTimeMillis();
    }

    // mendapatkan informasi waktu dengan parameter offset . untuk keperluan simulasi waktu
    public static long getTime(int ind_sesi) {
//        Logger.info("getTime() Dieksekusi oleh thread %s", Thread.currentThread().getName());
        Long offset=dateMap.get(ind_sesi);
//        if(offset == null)
//            offset = dateMap.get(0);
        if(offset == null)
            offset = 0L;
        return System.currentTimeMillis() + offset.longValue();
    }

    public static synchronized void setTime(long time)
    {
        setTime(0, time);
    }

    public static synchronized void setTime(int ind_sesi, long time)
    {
        long offset = time - getTime();
        dateMap.put(ind_sesi, offset);
    }
	
	/** Untuk melakukan simulasi, new Date() harus menggunakan fungsi ini. */
	public static Date newDate()
	{
		DateTime datetime = new DateTime(getTime());
		return datetime.toDate();
	}
	
	/** Dapatkan new Date() untuk sesi training tertentu */
	public static Date newDate(int ind_sesi)
	{
        DateTime datetime = new DateTime(getTime(ind_sesi));
        return datetime.toDate();
	}
	
	/** Dapatkan ganti current Date() untuk sesi tertentu */
	public static synchronized void setDate(Date newDate)
	{
		setTime(0, newDate.getTime());
	}
	
	public static synchronized void setDate(int ind_sesi, Date newDate)
	{
		setTime(ind_sesi, newDate.getTime());
	}
	
	/**  Parsing untuk mendapatkan Date dengan format DATE_TIME_FORMAT_SMALL: dd-MM-yyyy*/
	public static Date parseSmallDate(String date, String time) throws ParseException
	{
		date= date + " " + time;
		return org.apache.commons.lang.time.DateUtils.parseDate(date, new String[] {DATE_TIME_FORMAT_SMALL});
	}
	
	public static Date parseSmallDate(String dateTime) throws ParseException
	{
		return org.apache.commons.lang.time.DateUtils.parseDate(dateTime, new String[] {DATE_TIME_FORMAT_SMALL});
	}
	
	public static int getTahunSekarang() {
		DateTime datetime = new DateTime(getTime());
		return datetime.getYear();
	}
}

package models.jcommon.util;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class DoubleConverter implements JsonDeserializer<Double>{

	@Override
	public Double deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		try {
			String value = json.getAsString();
			if(value == null || value.isEmpty())
				return 0d;
			return Double.parseDouble(value);
		} catch (NumberFormatException e) {
			throw new JsonParseException(e);
		}
	}

}

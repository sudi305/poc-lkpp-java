package models.jcommon.secure.encrypt2;

import javax.crypto.Cipher;

import models.jcommon.secure.encrypt.CipherEngineException;

public class DecryptCipherEngine extends CipherEngine2 {

	public DecryptCipherEngine(String cipherKey) throws CipherEngineException {
		super(Cipher.DECRYPT_MODE, cipherKey);
	}

}

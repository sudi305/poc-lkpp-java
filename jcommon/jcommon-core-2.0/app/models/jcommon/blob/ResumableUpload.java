package models.jcommon.blob;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.file.OpenOption;
import java.util.Date;
import java.util.UUID;

import javax.persistence.EnumType;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.TempFileManager;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import play.Logger;
import play.Play;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**Class ini me-manage upload file yang resumable
 * 
 * @author Mr. Andik
 *
 */
@Table(name="RESUMABLE_UPLOAD")
public class ResumableUpload extends BaseTable{
	
	@Enumerated(EnumType.STRING)
	public enum UPLOAD_STATUS {
		INIT, PARTIAL, COMPLETED;
	}
	
	@Id
	public String id;
	
	public long received;
	
	public long file_size;
	
	public UPLOAD_STATUS status;
		
	public ResumableUpload(long fileSize) {
	
		this.file_size=fileSize;
	}
	public void prePersist()
	{		
		if(status==null)
		{
			status=UPLOAD_STATUS.INIT;
			id=UUID.randomUUID().toString();
			id=String.format("%1$ty%1$tm%1$td%1$tH-%2$s", new Date(), id);
		}
		super.prePersist();
	}
	
	public File getFile() throws IOException
	{
		//create if not exists
		
		return new File(TempFileManager.customTempFolder+"/" +  id);
			
	}
	
	
	/**
	 * Add InputStream to File
	 * @param length 
	 * @param end 
	 * @param start 
	 * @param eTag
	 * @throws IOException 
	 */
	public UPLOAD_STATUS addStreamToFile(InputStream is, int start, int end, int length) throws IOException	
	{
		byte[] buff=IOUtils.toByteArray(is);

		File file=getFile();
		try(RandomAccessFile raf=new RandomAccessFile(file, "rw"))
		{
			if(!file.exists())
				raf.setLength(length);//set file length
			else
				raf.seek(start);
			//write to random access file
//			Logger.debug("writing to file %s, size: %s, offset: %s", file, buff.length, start);
			raf.write(buff);
			raf.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		received+=buff.length;
		
		if(received==length)
		{
			//check MD5 of completed file
			status=UPLOAD_STATUS.COMPLETED;
		}
		else
			status=UPLOAD_STATUS.PARTIAL;
		save();
		return status;
	}
	
	public String getMD5Hash() throws FileNotFoundException, IOException
	{
		File file=getFile();
		String hash=DigestUtils.md5Hex(new FileInputStream(file));
		return hash;
	}
	
	
}

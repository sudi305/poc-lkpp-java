package models.jcommon.logger;

import java.sql.Timestamp;
import java.util.Date;

public class BaseActivityLog  {
	
	public Timestamp log_time; //info lengkap tentang tanggal & waktu
	public Date log_date;//info tanggal saja
	public String user_id;
	public String host;
	
	public String data;
	public String url;
	
	public String toString()
	{
		return user_id + "@" + host + "/" + url + ":" + data; 				
	}
}

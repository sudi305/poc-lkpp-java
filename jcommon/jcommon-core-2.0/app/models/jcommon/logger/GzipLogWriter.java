package models.jcommon.logger;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.zip.GZIPOutputStream;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang.time.DateUtils;

import play.Logger;

import com.google.gson.Gson;

/**Class ini menulis log ke database via SQL
 * Log tidak disimpan di database, namun di file  $CONFIG/file-storage-dir/A/yyyy/l0g/yyyy-mmdd.log 
 * JIka sudah lewat sehari akan di-convert menjadi .gz
 * 
 * TODO seharusnya file .log maupun .log.gz dienkripsi
 * TODO pastikan bahwa semua file .log telah dikonvert menjadi .log.gz
 * Ada kondisi tgl applikasi running lalu off mulai 20-01-2014 23:00 - 20-05-2014 06:00
 * Data log yang ada pada tanggal 20-01-2014 belum sempat di-gzip   
 */
public class GzipLogWriter extends AbstractLogWriter {
	
	public static String LOG_SUFFIX=".log";
	
	private String getFileName(Date date)
	{
		DateFormat dateFormat=new SimpleDateFormat("yyyy/'l0g'/yyyy-MMdd");
		String fileFormat="%s/%s/%s" + LOG_SUFFIX;
		String logFileName=String.format(fileFormat, BlobTableDao.getFileStorageDir(), 
				BlobTable.ARCHIEVE_MODE.ARCHIEVE.value, dateFormat.format(date));
		return logFileName;
	}
	
	public void doJob() {
//		Logger.debug("[GzipLogWriter].doJob");
		//Dapatkan hari ini dan siapkan file .gz dengan format yyyy-MMdd
		java.util.Date currentDate=DateUtils.truncate(new java.util.Date(), Calendar.DATE);
	
		String logFileName=getFileName(currentDate);
		
		PrintWriter writer=null;		
		try {
			
			//buka file			
			writer=openFileForAppend(logFileName);
		} catch (IOException e1) {
			e1.printStackTrace();
			return ;
		}
		
		Gson gson=new Gson();
		while(true)
		{
			try
			{	
				ActivityLog log= queue.poll();
				if(log!=null)
				{
			
					Date log_date=log.log_date;
					if(!currentDate.equals(log_date))
					{
						/**Telah Berpindah hari
						 * 1. Convert file menjadi gzip
						 * 2. Buat file baru
						 * 
						 * Konversi menjadi gzip dilakukan setelah satu file log diproduksi untuk satu hari.
						 * Ini menghasilkan file dengan ukuran lebih kecil jika dibandingkan ketika
						 * menggunakan 
						 * 		new PrintWriter(new GzipOutputStream(new FileOutputStream(fileName , true)));
						 */
						writer.close();
						
						doArchiveAsGzip(logFileName); 
						
						currentDate=log_date;
						logFileName=getFileName(currentDate);
						writer=openFileForAppend(logFileName);
					};
					//tulis ke file
					String data=gson.toJson(log);
					writer.println(data);					
				}
				else
					break; //tidak ada lagi log
			}
			catch(Exception e)
			{
				if(!(e instanceof java.lang.InterruptedException))
					Logger.error("[GzipLogWriter] Error while writing log: %s", e.toString());
			}
		}
		writer.close();
	}

	
	/**Lakukan konversi semua file log; kecuali hari ini
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * 
	 */
	public void doArchiveAllAsGzip() throws FileNotFoundException, IOException
	{
		
		try{
			File root=new File(BlobTableDao.getFileStorageDir());
			if(!root.exists())
				return;
			Collection<File> list=FileUtils.listFiles(root, new SuffixFileFilter(LOG_SUFFIX), new WildcardFileFilter("*"));
					
			if(list!=null)
				Logger.info("[GzipLogWriter] Archiving %s log files", list.size());
			String todayFile=FilenameUtils.getName(getFileName(new Date()));
			for(File file: list)
			{
				String fileName=file.toString();
				if(!fileName.endsWith(todayFile)) //kecuali file log hari ini
					doArchiveAsGzip(fileName);
			}
		}
		catch(NullPointerException e)
		{
			Logger.warn("CONFIG.storage-dir Is null");
		}
	}
	
	private void doArchiveAsGzip(String logFileName) throws FileNotFoundException, IOException {
		String gzipFile=logFileName + ".gz";
		File file=new File(logFileName);
		GZIPOutputStream gz=new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(gzipFile)));
		BufferedInputStream is=new BufferedInputStream(new FileInputStream(file));
		IOUtils.copy(is, gz);
		gz.close();
		is.close();
		//delete original
		file.delete();
	}

	private PrintWriter openFileForAppend(String fileName) throws IOException
	{
		File file=new File(fileName);
		file.getParentFile().mkdirs();
		return new PrintWriter(new BufferedOutputStream(new FileOutputStream(file , true)));
	}
	
}

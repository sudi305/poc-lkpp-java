package controllers.jcommon.logger;

import java.lang.annotation.Annotation;

public class DefaultLoggerSetting implements LoggerSetting {

	@Override
	public Class<? extends Annotation> annotationType() {
		return DefaultLoggerSetting.class;
	}

	@Override
	public String urlExcludePattern() {
		return "";
	}

	@Override
	public String paramExcludePattern() {
		return LoggerSetting.paramExcludeDefault;
	}

	@Override
	public String methodIncludePattern() {
		return "POST";
	}

	@Override
	public String userIdVariable() {
		return "userId";
	}

}

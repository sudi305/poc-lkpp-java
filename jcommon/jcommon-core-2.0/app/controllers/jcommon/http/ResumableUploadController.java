package controllers.jcommon.http;

import org.jboss.netty.handler.codec.http.HttpResponseStatus;

import models.jcommon.blob.ResumableUpload;
import models.jcommon.blob.ResumableUpload.UPLOAD_STATUS;
import models.jcommon.blob.ResumableUploadListener;
import play.Logger;
import play.Play;
import play.mvc.Controller;
import play.mvc.Http.Header;

/**Class untuk melakukan resumable Upload.
 * Protokol yang digunakan mirip dengan Resumable Download, yaitu memanfaatkan "Range" dan Method "PUT"
 * 
 * Fungsi dasar untuk upload sudah OK. File disimpan di folder /tmp/resumableUpload
 * 
 * //TODO Aspek security akan ditambahkan berikutnya.
 * 
 * @author Mr. Andik
 */
public class ResumableUploadController extends Controller {
	
	/**Cara penggunaan
	 * 1. [CLIENT] "POST /putfile/{md5Hash}"	=> md5Hash dari file yang diupload
	 * 		Request Header=> Range: * /{fileSize}
	 * 2. [SERVER] response header "ETag: {eTag}"
	 * 		{eTag} adalah nama file temporary yang akan digunakan
	 * 		File {eTag} akan tetap ada dan dihapus setelah periode tertentu atau setelah file berhasil diupload
	 * 3. [CLIENT]	
	 * 		"PUT /putfile/{md5Hash}/{eTag}"
	 * 		Request Header=> Range: {start}-{end}/{fileSize}
	 * 		{eTag} adalah etag yang sebelumnya didapat dari server
	 * 		{start}-{end} adalah urutan byte ke-sekian yang dikirim. Dimulai dari nol
	 * 4. [SERVER] 
	 * 		response status: "Partial Content"
	 * Langkah 3 & 4 berulang hingga seluruh byte terkirim
	 * 5. [SERVER]
	 * 		response status: "OK" jika seluruh data diterima dan MD5Hash sama
	 * 		response status: "FORBIDDEN" jika MD5Hash tidak sama antara yang dikirim dan yang diterima
	 * 		    
	 *  
	 *  
	 *  Note: 'Chunk' file yang diterima bisa jadi tidak berurutan
	 * @param fileId
	 * @param dateInfo
	 * @param eTag
	 * @throws Exception
	 */
	public static void putFile(String md5Hash, String eTag) throws Exception
	{
		if(eTag==null)//means new session, returns E-Tag
		{
			Header rangeHeader=request.headers.get("range");
			long fileSize=Long.parseLong(rangeHeader.value().split("/")[1]);
			ResumableUpload um=new ResumableUpload(fileSize);
			um.save();
			eTag=um.id;
			response.headers.put("ETag", new Header("ETag", eTag));
			Logger.debug("[SERVER] INIT range: %s",rangeHeader);
			response.status=org.jboss.netty.handler.codec.http.HttpResponseStatus.OK.getCode();
		}
		else
		{	
			ResumableUpload um=ResumableUpload.findById(eTag);
			Header rangeHeader=request.headers.get("range");
			if(rangeHeader==null)
				throw new IllegalArgumentException("Range header is missing");	
			String ary[]=rangeHeader.value().split("[-|//]");
			int start=Integer.parseInt(ary[0]);
			int end=Integer.parseInt(ary[1]);
			int length=Integer.parseInt(ary[2]);
		
//			Logger.debug("[SERVER] eTag %s, range %s", eTag, rangeHeader.value());
			UPLOAD_STATUS status= um.addStreamToFile(request.body, start, end, length); 
			if(status==UPLOAD_STATUS.COMPLETED)
				//write status 
			{
				String className=Play.configuration.getProperty("resumableUploadListener.class");
				if(className==null)
					throw new IllegalArgumentException("resumableUploadListener.class is null");
				Class<ResumableUploadListener > cls=(Class<ResumableUploadListener>) Class.forName(className);
				ResumableUploadListener listener= cls.newInstance();

				//check MD5 value
				String md5Server=um.getMD5Hash();
				if(md5Server.equalsIgnoreCase(md5Hash))
				{
					
					listener.uploadDone(um);
					response.status=org.jboss.netty.handler.codec.http.HttpResponseStatus.OK.getCode();
				}
				else
				{
					response.status=org.jboss.netty.handler.codec.http.HttpResponseStatus.FORBIDDEN.getCode();
					String msg="MD5Hash do not match.MD5 of received file: " + md5Server;
					listener.uploadFailed(um, msg);
					renderText(msg);
				}
			}
			else
				response.status=HttpResponseStatus.PARTIAL_CONTENT.getCode();
		}
	}

	

}

package controllers.jcommon.blob;

import java.io.File;
import java.io.FileNotFoundException;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.blob.BlobTableSecureId;
import play.libs.MimeTypes;
import play.mvc.Controller;
import play.mvc.Http.Header;

/**Class ini digunakan untuk download file dengan id & versi tertentu.
 * Controller harus memastikan bahwa orang tidak donwnload file yang salah 
 * baik secara sengaja atau tidak.
 * 
 * @author Andik Yulianto
 */
public class FileDownloader extends Controller {
	
	/**
	 * Download file
	 * TODO: Bagaimana handling file-file yang lama? Harus migrasi?
	 * @param id merupakan models.jcommon.blob.SecureId.id
	 * yang berisi encrypted-version dari ID BlobTable.
	 * Di dalamnya terkandung juga instace dari interface DownloadSecurity yang harus di-implement oleh 
	 * aplikasi.
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws FileNotFoundException 
	 */
	public static void download(String id) throws ClassNotFoundException, InstantiationException, IllegalAccessException, FileNotFoundException {
		//remove extension
		int pos=id.indexOf('.');
		if(pos>0)
			id=id.substring(0, pos);
		BlobTableSecureId secureId = new BlobTableSecureId(id);
		DownloadSecurityHandler handler = secureId.getDownloadSecurityHandler();
		BlobTable blob = BlobTableDao.findById(id);
		if(!handler.allowDownload(blob)) {
			forbidden("Access to file is forbidden");
		}
		if(!blob.isMirrored())
		{
			File file = blob.getFile();
			//renderBinary(file);//pakai method play!
			response.contentType=MimeTypes.getContentType(blob.blb_nama_file, "application/octet-stream");
			Header header=new Header("Content-Disposition", "attachment; filename=\"" + blob.blb_nama_file + "\"");
			response.headers.put("Content-Disposition",header);
			renderBinary(file);
		}
		else
			redirect(blob.blb_url, true);
	}
		
}

package models.workflow.instance;

import java.util.Date;

import models.workflow.WorkflowBaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="WF_PROCESS_INSTANCE_LOG")
public class ProcessInstanceLog extends WorkflowBaseModel{
	
	@Id(sequence="SEQ_WF_PROCESS_INSTANCE_LOG")
	public Long logId;	

	public Long process_instance_id;
	
	public Date log_date;

	public String log_info;
	
}

package models.workflow;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import play.db.jdbc.BaseTable;
import play.mvc.Scope.Session;

@MappedSuperclass
public class WorkflowBaseModel extends BaseTable {

	public String audittype;
	
	public String audituser;
	
	public Date auditupdate;
	
	public Date createdate;

	@Override
	protected void prePersist() {
		audittype = audittype != null && !audittype.isEmpty() ? "U" : "C";
		Session current_session = Session.current();
		if (current_session != null) {
			audituser = current_session.get("userId"); // set audituser
			if (audituser == null)
				audituser = "N/A";
		} else { // handling jika session object tidak ada, misal saat mengerjakan JOB
			audituser = "_UNKNOWN_";
		}
		auditupdate = new Date();
	}

}

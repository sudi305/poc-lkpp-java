package controllers.workflow;

import java.util.Date;
import java.util.List;
import java.util.Map;

import models.workflow.Process_instance_detail;
import models.workflow.definition.ProcessDefinition;
import models.workflow.definition.Workflow;
import models.workflow.instance.ProcessInstance;
import play.Play;
import play.data.FileUpload;
import play.mvc.Before;
import play.mvc.Controller;

public class WorkflowBrowser extends Controller {

	private static boolean WORKFLOW_BROWSER_ENABLED = Boolean.valueOf(Play.configuration.getProperty("workflow.browsing.enable", "false")).booleanValue();
	
	@Before
	public static void checkAll() {
		if(!WORKFLOW_BROWSER_ENABLED)
			forbidden("Workflow Manager tidak aktif");
		renderArgs.put("editable", Play.mode.isDev());
	}
	/**
	 * Workflow browser. Untuk mengaktifkan
	 * Cache.add("jcommon.workflow.browsing.enable",true);
	 */
	public static void index() { 
		List<Process_instance_detail> listPI = Process_instance_detail.findAll();
		render(listPI);
	}

	/** Cari proses yang punya descripsi tertentu */
	public static void searchProcess(String description) {
		List<ProcessInstance> listPI = ProcessInstance.findByDescription(description);
		render("workflow/WorkflowBrowser/index.html", listPI,description);
	}

	public static void showProcessDefinition(Integer pdId) {
		ProcessDefinition pd = ProcessDefinition.findById(pdId);
		render(pd);
	}

	/**
	 * Tampilkan process instance. Tombol submit muncul hanya jika pada mode DEV
	 */
	public static void showProcessInstance(Long piId) {
		ProcessInstance pi = ProcessInstance.findById(piId);
        Date date = new Date();
		render(pi, date);
	}

	/** Update variable */
	public static void processInstanceUpdateVariables(Long piId) throws Exception {
		checkAuthenticity();
		ProcessInstance pi = ProcessInstance.findById(piId);
		// get variables
		Map<String, String[]> variables = params.all();
		pi.updateVariablesValue(variables);
		showProcessInstance(pi.process_instance_id);
	}

	/**
	 * Upload process definition, Status sebagai DRAF
	 * 
	 * @param fileProcessDefinition
	 * @throws Exception
	 */
	public static void uploadProcessDefinitionSubmit(FileUpload fileProcessDefinition) throws Exception {
		if (fileProcessDefinition != null)
			Workflow.importProcessDefinition(fileProcessDefinition.asStream(), true);
		listProcessDefinition();
	}

	public static void listProcessDefinition() {
		List<ProcessDefinition> listPD = ProcessDefinition.order("process_definition_id asc").fetch();
		render(listPD);
	}

	public static void testProcessDefinition(Integer processDefinitionId) {
		ProcessDefinition pd = ProcessDefinition.findById(processDefinitionId);
		render(pd);
	}

	public static void testProcessDefinitionSubmit(Integer processDefinitionId,
			FileUpload fileUpload) throws Exception {
		ProcessDefinition pd = ProcessDefinition.findById(processDefinitionId);
		if (fileUpload != null)
			pd.testProcessDefinition(processDefinitionId, fileUpload.asStream());
		testProcessDefinition(processDefinitionId);
	}
	
	public static void deleteProcessInstance(Long processInstanceId) {
		if(Play.mode.isDev()) {
			ProcessInstance pi = ProcessInstance.findById(processInstanceId);
			pi.delete();
		}
		index();
	}
	
	public static void deleteProcessDefinition(Integer procesDefinitionId) {
		if(Play.mode.isDev()) {
			ProcessDefinition pd = ProcessDefinition.findById(procesDefinitionId);
			pd.delete();
		}
		listProcessDefinition();
	}

}

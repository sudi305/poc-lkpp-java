h1. Petunjuk Penggunaan Workflow Engine

Workflow Engine berfungsi untuk mengatur __flow__ dari sebuah aplikasi. Elemen dari Workflow Engine

#  Workflow
#  Process Definition
#  State
#  Variable
#  StateMapping
#  Process Instance
#  State Instance
#  Variable Instance


p. Satu Workflow (models.workflow.definition.Workflow) menyimpan satu alur kerja.
Di dalamnya terdapat minimal satu Process Definition (models.workflow.definition.ProcessDefinition) dengan versi tertentu. 
Jika ada versi yang baru maka ketika ProcessInstance dibuat akan menggunakan ProcessDefinition dengan versi terbaru.

h2. Format File Process Definition

p. Satu Process Definition (PD) merupakan kumpulan dari State, Variable, dan StateMapping. Process Definition dibuat pada file dengan format CSV. Berikut ini contohnya.

| 1 | 	PASCA_1S_GUGUR	|	1	|	Pasca Kualifikasi 1 sampul sistem gugur	|	
| 2	|	Current State	|	Next State	|	Condition	|	SetVariable
| 3	|   --START-- |	@PERSIAPAN	|		|	
| 4	|	PERSIAPAN	|	PENGUMUMAN_LELANG	|	NOW>= TGL_PENGUMUMAN_START	|	
| 5	|	PERSIAPAN	|	PENJELASAN_LELANG	|	NOW>= TGL_PENJELASAN_START	|	
| 6	|	PERSIAPAN	|	UL_DOK_PENAWARAN	|	NOW>= TGL_UPLOAD_DOK_PENAWARAN_START	|	
| 7	|	PERSIAPAN	|	DL_DOK_LELANG	    |	NOW>= TGL_DOWNLOAD_DOK_LELANG_START	|	
| 8	|	PENJELASAN_LELANG	|	UL_DOK_PENAWARAN	|	NOW>= TGL_PENJELASAN_END	|	
| 	|		|		|		|	
| 20	|	EVAL_PENAWARAN	|	PENGUMUMAN_DAN_SANGGAH	|	EVAL_PENAWARAN_BERAKHIR	|	EVAL_PENAWARAN_BERAKHIR=false; EVALUASI_ULANG;
| 	|	....	|	....	|	...	|	
| 21	|	GAGAL	|	EVAL_PENAWARAN	|	EVALUASI_ULANG	|	
| 22	|	GAGAL	|	UL_DOK_PENAWARAN	|	PENAWARAN_ULANG	|	
| 23	|	GAGAL	|	LELANG_ULANG	|	LELANG_ULANG	|	
| 	|	...	|		|		|	
| ..	|	END	|		|		|	
--

...
#  Terdiri atas 4 kolom
#  *Baris 1* : PASCA_1S_GUGUR merupakan Workflow_Id, angka 1 menunjukkan versi dari Process Definition ini, sedang kolom ketiga merupakan deskripsi Process Definition.
#  *Baris 2* : Column header
#  *Baris 4* : dan seterusnya disebut State Mapping
#  Jika teks pada kolom pertama yang diawali tanda # akan dianggap komentar dan diabaikan.
#  Setiap PD harus diawali dengan Current State START
#  Perpindahan dari satu Current State ke Next State terjadi ketika Condition terpenuhi. Pada baris 4 misalnya, jika variable NOW >= variabel TGL_PENGUMUMAN_START maka state PENGUMUMAN_LELANG akan menjadi aktif sedang State PERSIAPAN menjadi non-aktif (kecuali jika PERSIAPAN sebagai FORK, lihat penjelasan di bawah)
#  Setelah berpindah ke Next State, akan di-call <em>SetVariable</em> yaitu isi dari kolom ke-4. SetVariabel ini penting agar Variable-variable tidak menyebabkan looping yang salah.Contoh, ketika keluar dari state EVAL_PENAWARAN, makavariable EVAL_PENAWARAN_BERAKHIR, akan diset false.Ketika nantinya terjadi EVALUASI_ULANG, state akan berhenti pada EVAL_PENAWARAN.
#  Semua State harus sampai ke State END. Jika tidak, workflow akan menampikan pesan error ketika Process Definition (file CSV) diimpor ke database.
#  File Process Definition diimpor ke database oleh method *models.workflow.definition.dao.WorkflowDao.importProcessDefinition(InputStream is, boolean replaceExisting)*. Jika replaceExisting true, maka menimpa Process Definition yang lama asalkan belum ada Process Instance yang menggunakannya.

h3. Ketentuan  Variable dan  State

p. 
#  Nama Variable dan nama State terdiri atas huruf besar dan underscore
#  State yang diawali tanda @ menunjukkan bahwa dia sebagai FORK. Pada contoh di atas, state PERSIAPAN akan tetap aktif hingga State Mapping nomor 4 sampai nomor 7 terpenuhi.
#  Variable dengan awalan TGL_ merupakan variable bertipe Date.
#  Variable NOW merupakan variable khusus bertipe Date yang menunjukkan waktu saat ini.
#  Variable bertipe Date harus diisi dengan format yyy-MM-dd HH-mm. (HH artinya jam 0-23)

h3. Cara Penggunaan


#  Siapkan Process Definition pada file CSV lalu import ke Database.
#  Call method *ProcessInstanceDao.createInstance(WorkflowId, description)*. Sistem akan menggunakan Process Definition terakhir pada WorkflowID tersebut. Isikan decription dengan informasi yang relevan.
#  Isikan nilai dari semua variabel dengan method *processInstance.setVariable()*.Jika ada variabel yang tidak diinisiasi maka akan muncul error ketika variabel dievalusi oleh State Mapper.
#  Call *processInstance.startInstance()* untuk memulai process berjalan.
#  Untuk berpindah ke state berikutnya pastikan untuk mengubah variable-variable yang diperlukan lalu call *processInstance.gotoNextState()*. Ubah variable NOW untuk memberitahu Workflow tentang waktu saat ini.
#  Process Instance memiliki status CREATED, RUNNING, COMPLETED. processInstanceDao.createInstance() membawa status menjadi CREATED. startInstance() mengubah status menjadi RUNNING. Ketika sudah sampai pada state END akan berubah menjadi COMPLETED.


h2. Testing dan Simulasi

p. Pada aplikasi workflow-test terdapat method *CreateProcessInstance.runTest(testScenario).* Ini digunakan untuk melakukan testing dan simulasi terhadap Process Definition. Untuk menjalankannya diperlukan file CSV *test-scenario* dengan format sebagai berikut:

#  Terdiri atas 3 kolom yaitu ACTION, VARIABLE, VALUE
#  Jika teks pada kolom pertama kosong atau diawali # akan dianggap komentar dan diabaikan.
#  Action: *createWorflow, description, WorkflowID*<br />Action ini harus merupakan action yang pertama kali dibuat. Sistem akan membuat Process Instance dengan Workflow ID tersebut.
#  Acton: *setVariable, namaVariable, value*<br />Action ini bertujuan mengisi namaVariable dengan value tertentu. Untuk variable Date, pastikan dengan format yyyy-MM-dd HH:mm.
#  Controller *controllers.workflow.WorkflowBrowser* berisi halaman-halaman untuk melihat workflow.
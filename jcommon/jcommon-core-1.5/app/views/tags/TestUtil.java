package views.tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import play.Logger;
import play.Play;
import play.db.jdbc.Query;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

public class TestUtil extends FastTags {
	
	/** Import postgres db into database.
	 * Parameter pgRestore: full path for pg_restore.exe
	 * file: File .backup to restore, if null means only truncate database
	 * console: true/false: show/hide console  output */
	public static void _importIntoDatabase(Map<?, ?> args, Closure body, PrintWriter out,
			ExecutableTemplate template, int fromLine) {
		String backupFile=(String) args.get("file");
		String pgRestore=(String) args.get("pgRestore");
		Boolean console=false;
		if(args.get("console")!=null)
		{
			console=(Boolean) args.get("console");
		}
		List<String> list= Query.find("select table_name from information_schema.tables where table_schema='public'", String.class).fetch();
		
		for(String str: list)
		{			
			String sql="TRUNCATE " + str + " CASCADE";
			Query.update(sql);
		}
		if(backupFile==null)
		{
			Logger.info("Database truncated. No file was imported");
			return;
		}
			//jdbc:postgresql://10.1.31.31:5433/sso-lpse-4			
			String url=Play.configuration.getProperty("db.url");
			String host=url.substring(18, url.lastIndexOf(":"));
			String port=url.substring(url.lastIndexOf(":")+1,url.lastIndexOf("/"));
			String dbName=url.substring(url.lastIndexOf("/")+1);
			String user=Play.configuration.getProperty("db.user");
			String password=Play.configuration.getProperty("db.pass");
			//C:\Program Files\PostgreSQL\9.1\bin\pg_restore.exe --host 10.1.31.31 --port 5433 --username postgres --dbname "sso-lpse-4" --verbose "D:\Bappenas\eproc4\sso\sql\db-test-data\single-nip-banyak-userId.backup"
			String program=String.format("\"%s\" --host %s --port %s --username %s --dbname \"%s\" --verbose \"%s\"", pgRestore, host, port, user, dbName, backupFile);
			try {
				
				ProcessBuilder pb = new ProcessBuilder(program);
				if(console)
				{
					pb.redirectErrorStream();
				}
				else
				{
					pb.redirectErrorStream(false);
				}
				pb.start().waitFor();		
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
}

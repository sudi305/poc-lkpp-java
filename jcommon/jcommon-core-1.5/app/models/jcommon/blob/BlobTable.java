package models.jcommon.blob;


import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import org.sql2o.ResultSetHandler;

import models.jcommon.config.Configuration;
import models.jcommon.db.base.BaseModel;
import play.Logger;
import play.db.DB;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.mvc.Http.Header;
import play.mvc.Http.Request;
import play.mvc.Router;
import controllers.jcommon.blob.DefaultDownloadHandler;
import controllers.jcommon.blob.DownloadSecurityHandler;

/**
 * BlobTable harus memastikan bahwa siapapun tidak bisa mengakses file sistem secara fisik
 * untuk menghapus atau mengganti file tertentu.
 * Oleh karena itu, dari table BLOB_TABLE tidak akan dapat diketahui file secara fisik yang mana.
 * Caranya: Informasi path, nama file, ukuran disimpan pada blb_path secara terenkripsi
 * Namun demikian masih ada kemungkinan orang bisa tahu dari atribut last_modified time/create time
 * Sayangnya, java tidak bisa mengubah atribut ini
 * 
 * Extends ke generic model agar tidak ada info auditupdate.
 */
@Table(name="blob_table")
public class BlobTable extends  BaseModel {
	
	public static final String FILE_STORAGE_DIR = "file.storage-dir";
	public static final String FILE_STORAGE_DIR_ALT="file-storage-dir";
	public static final String ENGINE_VERSION="4.0.0";

	public static enum ARCHIEVE_MODE {
		ARCHIEVE('A'), NON_ARCHIEVE('S');
		ARCHIEVE_MODE(char value)
		{
			this.value=value;
		}
		public final char value;
	}
	
	public static String getFileStorageDir()
	{
		String fs=Configuration.getConfigurationValue(FILE_STORAGE_DIR);		
		if(fs==null)
		{
			fs=Configuration.getConfigurationValue(FILE_STORAGE_DIR_ALT);
			if(fs==null)
				throw new IllegalArgumentException(FILE_STORAGE_DIR + " kurang lengkap.");
		}
		//khusus untuk 'file-storage-dir', bisa berlaku untuk windows/linux. Contoh: "/home/file/file_latihan;C:\temp"
		String[] ary = (fs + "; ").split(";"); // pastikan minimal ada 1 buah ';'
		String windowsPath = !ary[0].startsWith("/") ? ary[0] : ary[1];
		String unixPath = ary[0].startsWith("/") ? ary[0] : ary[1];
		if (File.separatorChar == '\\')// windows
			fs = windowsPath;
		else
			fs = unixPath;
		return fs;
	}
	
	
	@Id
	public Long blb_id_content;

	@Id	
	public Integer blb_versi;

	public Date blb_date_time;

	/**
	 * Path file ter-enkripsi
	 */
	public String blb_path;
	
	public String blb_engine;
	
	@Transient
	private File file;
	
	@Transient
	public String blb_nama_file;
		
	@Transient
	public String blb_hash;
	
	//Jika blb_mirrored= 1 maka ini adalah lokasi CDN yang dimaksud
	public Long cdn_host_id;
	
	@Transient
	public String blb_url;
	
	public Integer blb_mirrored;
	
	public Long blb_ukuran;


	/**
	 * Dapatkan blob_table dengan id & versi tertentu
	 * 
	 * @param id_content
	 * @param versi
	 * @return
	 */
	public static BlobTable findById(long id_content, int versi)
	{
		return find("blb_id_content=? and blb_versi=?", id_content, versi).first();
	}
	
	public String toString()
	{
		return "[" + blb_id_content + "] " + blb_path;
	}

	public File getFile() {
		return file;
	}

	public String getFileName() {
		return blb_nama_file;
	}
	
	public void postLoad()
	{
		if(blb_engine==null)
		{
			try
			{
				//ini versi lama, blb_path berisi path asli, so ambil via SQL
				Query.find("select blb_path, blb_nama_file, blb_hash from  BLOB_TABLE WHERE BLB_ID_CONTENT=? and blb_versi=?", new ResultSetHandler<String>() {

					@Override
					public String handle(ResultSet rs) throws SQLException {
						blb_path=rs.getString(1);
						blb_nama_file=rs.getString(2);			
						blb_hash=rs.getString(3);
						return null;
					}
				}, blb_id_content, blb_versi).first();
				
				file=new File(getFileStorageDir() + "/" + blb_path + "/" + blb_nama_file);
			}
			catch(Exception e)
			{
				Logger.error(e, "Error PostLoad models.jcommon.blob.BlobTable");
			}
		}
		else
		{
			
			String pathString=decrypt(blb_path);		
			//path: path,hash,name,size,date_created (separated by \n)
			String aryPath[]=pathString.split("\n");
			try
			{
				if(blb_mirrored!=null && blb_mirrored==1)
					file=null;
				else
				file=new File(getFileStorageDir() + "/" + aryPath[0]);
				blb_hash=aryPath[1];
				blb_nama_file=aryPath[2];				
				blb_url=aryPath[0];
				blb_ukuran=Long.parseLong(aryPath[3]);
				
			}
			catch(Exception e)
			{				
				Logger.error("ERROR while getting path. [%s, %s] pathString %s", blb_id_content, blb_versi, pathString);
//				throw new Exception(String.format("Error parsing path for [%s, %s] %s",  blb_id_content, blb_versi, pathString));
			}
		}
	}
	
	private String getId(Class downloadSecurityHandler) throws IllegalAccessException
	{		
		if(downloadSecurityHandler==null)
			downloadSecurityHandler=DefaultDownloadHandler.class;
		String id=new BlobTableSecureId(blb_id_content, blb_versi, downloadSecurityHandler).id;
		return id;
	}
	
	
	/**
	 * Menghapus semua versi blob yang memiliki id_content sama dengan this.
	 * Ada potensi inkonsistensi antara filesystem dan database
	 * @return
	 */
	public void deleteAllVersion()
	{
		List<BlobTable> list=BlobTableDao.listById(blb_id_content);
		for(BlobTable blb:list)
			blb.delete();
	}
	
	/** Menghapus blob ini */
	public void preDelete()
	{
		//delete current file
		try {
			postLoad();
		} catch (Exception e1) {
			e1.printStackTrace();
		}//init file
		file=getFile();
		if(file==null)
			return;
		try
		{
			if(file.exists())
				file.delete();
		}
		catch(Exception e)
		{
			Logger.error("Error deleting file: %s. '%s'", file.toString(), e.toString());
			e.printStackTrace();
			throw new RuntimeException("File cannot be deleted");
		}
		
	}

	/** This method will save and commit this model away from JPA.
	 *  */
	public void saveAndCommit() {		
//		em().detach(this);
//		EntityTransaction tx=em().getTransaction();
		String sql="UPDATE blob_table SET blb_path=?, blb_engine=? where blb_id_content = ? and blb_versi= ?";
//		javax.persistence.Query query=em().createQuery(sql);
		String path=BlobTableDao.encryptPathInfo(blb_path, blb_hash, file.getName(), String.valueOf(file.length()));
//		query.setParameter("path", path);
//		query.setParameter("engine", blb_engine);
//		query.setParameter("id", blb_id_content);
//		query.setParameter("versi", blb_versi);
//		if(!tx.isActive())
//			tx.begin();
//		query.executeUpdate();
//		tx.commit();
		Query.update(sql, path, blb_engine, blb_id_content, blb_versi);
	}

	public boolean isMirrored() {
		if(blb_mirrored==null)
			return false;
		return blb_mirrored==1;
	}
	
	public String getDownloadUrl(Class<? extends DownloadSecurityHandler> downloadSecurityHandler) 
	{
		try {
			Map<String, Object> params=new HashMap();
			String id=getId(downloadSecurityHandler);
			params.put("id", id);
//			Request request =Request.current();
		//	String url=Router.getFullUrl("jcommon.blob.FileDownloader.download", params);
//			Header header = request.headers.get("x-forwarded-host");
//			if(header==null)
//				header=request.headers.get("X-Forwarded-Host"); //takutnya stringnya spt ini
//			String host = "";
//			if(header==null)
//				host=request.host;
//			else
//				host = header.value();
//			String http = request.secure ? "https://":"http://";
			return Router.reverse("jcommon.blob.FileDownloader.download", params).url;
			/* tambahkan extension supaya dikenali oleh download manager
			 * Extensi untuk sementara di-disable karena error ketika URL di-parse oleh secure url Router
			String ext=FilenameUtils.getExtension(blb_nama_file);
			url+="." +ext;
			*/
		}catch(IllegalAccessException e) {
			e.printStackTrace();
			Logger.error("Illegal Access blobTable : %s", e.getMessage());
			return null;
		}
	}
}

package models.jcommon.blob;

import java.util.Random;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.DateUtil;
import play.Logger;
import controllers.jcommon.blob.DownloadSecurityHandler;

/**
 * This is secure ID for BlobTable. Used to encrypt download url
 * @author andik
 *
 */
public class SecureId {
	
	public Long id_content;
	public Integer versi;
	public String id;
	public String downloadSecurityHandlerClassName;
	
	//Constructor untuk create id dari string (decrypt mode)
	public SecureId(String id)
	{
		id=BaseModel.decrypt(id);
		//id tersusun atas [blb_id_content, blb_versi, currentDate, random number, downloadSecurityHandler]
		String ary[]=id.split("\\,");
		id_content=Long.parseLong(ary[0]);
		versi=Integer.parseInt(ary[1]);
		downloadSecurityHandlerClassName=ary[4];		
	}
	
	/**Create instance of security handler 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException */
	public DownloadSecurityHandler getDownloadSecurityHandler() throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		if(downloadSecurityHandlerClassName==null)
		{
			Logger.error("No security handler for BlobTable");
			return null;
		}
		Class cls=Class.forName(downloadSecurityHandlerClassName);
		Object obj= cls.newInstance();
		if(!(obj instanceof DownloadSecurityHandler))
		{
			throw new IllegalAccessException("Id is not instance of controllers.jcommon.blob.DownloadSecurityHandler");
		}
		DownloadSecurityHandler handler= (DownloadSecurityHandler)obj;
		return handler;
	}
	
	
	/** Constructor untuk create id dari string (encrypt mode)
	 * @param downloadSecurityHandler yaitu class yang akan melakukan pengecekan
	 * ketika proses download -> diijinkan atau tidak untuk download
	 */
	public SecureId(Long id_content, Integer versi, Class downloadSecurityHandler)
	{
		this.id_content=id_content;
		this.versi=versi;
		//id tersusun atas [blb_id_content, blb_versi, currentDate, random number, downloadSecurityHandler]
		Random rnd=new Random();
		this.downloadSecurityHandlerClassName=downloadSecurityHandler.getCanonicalName();		
		id=String.format("%s,%s,%s,%s,%s", id_content, versi, DateUtil.newDate().getTime(), rnd.nextLong(), this.downloadSecurityHandlerClassName);
		id=BaseModel.encrypt(id);		
	}
}

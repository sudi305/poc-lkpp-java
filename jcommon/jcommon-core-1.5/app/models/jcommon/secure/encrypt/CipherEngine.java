package models.jcommon.secure.encrypt;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import play.Logger;
import play.Play;
import play.libs.IO;

/**Class ini berfungsi untuk enkrip/dekrip dengan asymetric key 
 * Enkrip menggunakan public key file /conf/public_key
 * Enkrip menggunakan private key file /conf/private_key
 * Kedua key tersebut TIDAK boleh berpasangan.
 * Contoh penggunaan Transfer DARI SPSE => INAPROC
 * 1. Pada SPSE terdapat public_key A
 * 2. Ketika akan mengirim data ke Inaproc maka dienckripsi dengan public_key A
 * 3. Inaproc menggunakan class ini juga. Dia punya private_key A untuk dekripsi data tsb
 * 
 * Kedua file tersebut di-generate oleh @see models.jcommon.secure.encrypt.CipherEngine.generateKeyPair(String, String) 
 * 
 * Transfer dari INAPROC ke SPSE
 * 1. Enkripsi dengan public_key B di Inaproc
 * 2. Dekripsi dengan private_key B di LPSE
 * 
 * NOTE data minimal 16 byte
 * Class ini tidak dipakai lagi dan diganti CipherEngine2 karena kurang fleksibel
 *  
 * @author andik
 */
@Deprecated 
public class CipherEngine {
	private Key publicKey;
	private Key privateKey; 

	private Cipher cipherEncrypt;
	private Cipher cipherDecrypt;
	
	private static final String ALGORITHM="AES";
	
	//version of this engine
	public static final String VERSION="1.0.0";

	
	/** Generate public/private key and save to file
	 * @throws CipherEngineException 
	 * @throws IOException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchProviderException */
	public static void generateKeyPair(String pubKeyFileName, String privateKeyFileName) throws CipherEngineException 
	{
		try
		{
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);
			KeyPair kp= kpg.generateKeyPair();
//			KeyPairModel model=new KeyPairModel(kp);
			
//			IO.writeContent(model.publicKey, new File(pubKeyFileName));
//			IO.writeContent(model.privateKey, new File(privateKeyFileName));
		}
		catch(Exception e)
		{
			throw new CipherEngineException(e);
		}
	}
	
	/**Generate keypair: 
	 * 	arg[0] publicKeyFileName
	 * 	arg[1] privateKeyFileName
	 * 
	 * @param args
	 * @throws CipherEngineException 
	 */
	public static void main(String[] args) throws CipherEngineException
	{
		if(args.length!=2)
			System.out.println("Usage: java models.jcommon.secure.encrypt.CipherEngine PUB_KEY_FILENAME, PRIV_KEY_FILENAME");
		else
		{
			generateKeyPair(args[0], args[1]);
			System.out.format("KeyPair generated:\n%s\n%s", args[0], args[1]);
		}
	}
	
	/**Constructor dengan private/public key default:
	 * A. Jika pada application.conf terdapat entry 'private_key' dan/atau 'public_key'
	 *     maka gunakan file yang ditunjuk oleh entry tsb
	 * B. Jika (A) tidak ada maka gunakan file ini sbg key
	 * 	  - /conf/private_key & 
	 * 	  - /conf/public_key
	 * 
	 * @throws CipherEngineException
	 */
	public CipherEngine() throws CipherEngineException
	{
		//#1 
		File filePrivate=new File(Play.applicationPath + File.separator + Play.configuration.getProperty("private_key"));
		File filePublic=new File(Play.applicationPath + File.separator + Play.configuration.getProperty("public_key"));
		
		if(!filePrivate.exists())
			filePrivate=new File(Play.applicationPath + "/conf/private_key");
		if(!filePublic.exists())
			filePublic=new File(Play.applicationPath + "/conf/public_key");
		init(filePrivate, filePublic);
	}
	
	/**Constructor dengan private/public key yang ditentukan
	 * 
	 * @throws CipherEngineException
	 */
	public CipherEngine(File privateKeyFile, File publicKeyFile) throws CipherEngineException
	{
		init(privateKeyFile, publicKeyFile);
	}
	
	private void init(File privateKeyFile, File publicKeyFile) throws CipherEngineException
	{
		BouncyCastleProvider provider=new BouncyCastleProvider();
		Security.addProvider(provider);

		String privKey=null;
		try
		{
			privKey=IO.readContentAsString(privateKeyFile);
		}
		catch(Exception e)
		{
			Logger.warn("'private_key' tidak ditemukan. ChiperEngine tidak bisa untuk dekripsi.");
		}
		/*String pubKey=null;
		
		try
		{
			pubKey=IO.readContentAsString(publicKeyFile);
		}
		catch(Exception e)
		{
			Logger.warn("'private_key' tidak ditemukan. ChiperEngine tidak bisa untuk enkripsi.");
		}
		KeyPairModel kpm=new KeyPairModel(privKey, pubKey );
		try
		{
		privateKey=(Key)kpm.getPrivateKeyObject();		
		publicKey=(Key)kpm.getPublicKeyObject();
		}
		catch(Exception e)
		{
			throw new CipherEngineException(e);
		}*/
	}
	
	/**Dapatkan cipher untuk encrypt.
	 * Jika null, artinya tidak /conf/private_key tidak ada */
	private Cipher getCipherForEncrypt() throws CipherEngineException
	{
		if(publicKey==null)
			return null;
		 if(cipherEncrypt==null)
		 {			 
			 try {
				cipherEncrypt = Cipher.getInstance("RSA");			
				cipherEncrypt.init(Cipher.ENCRYPT_MODE, publicKey);
			} catch (Exception  e) {
				throw new CipherEngineException(e);
			}			 			 			
		 }
		 if(cipherEncrypt==null)
			 throw new CipherEngineException("/conf/public_key is not defined");
		 return cipherEncrypt;
	}
	
	/**Dapatkan cipher untuk decrypt
	 * Jika null, artinya tidak /conf/private_key tidak ada
	 * 
	 */
	private Cipher getCipherForDecrypt() throws CipherEngineException
	{
		if(privateKey==null)
			return null;
		if(cipherDecrypt==null)
		{
	        try {
				cipherDecrypt = Cipher.getInstance("RSA");
				cipherDecrypt.init(Cipher.DECRYPT_MODE, privateKey);
			} catch (Exception   e) {
				throw new CipherEngineException(e);
			}
	     
		}
		 if(cipherDecrypt==null)
			 throw new CipherEngineException("/conf/public_key is not defined");
        return cipherDecrypt;
	}
	

	/**Decrypt string.
	 * @param str in Base64 format */
	public String decryptString(String str) throws CipherEngineException {
		byte arySt[]=Base64.decodeBase64(str);
		
		ByteArrayInputStream is=new ByteArrayInputStream(arySt);
		ByteArrayOutputStream out=new ByteArrayOutputStream(str.length() * 2);
		try {
			decryptStream(is, out);
		}
		catch(Exception e )
		{
			throw new CipherEngineException(e);
		} 
		
		String result=out.toString();
		//buang 16 digit pertama
		result=result.substring(16);
		return result;
	}	
	
	public void decryptStream(InputStream is,
			OutputStream out) throws IOException, CipherEngineException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException {
		//2. Read version
		byte aryVersion[] = new byte[VERSION.length()];
		is.read(aryVersion);
//		String version=new String(aryVersion);
//		Logger.debug("CipherEngine Version: %s", version);
		
		//3. Read key (256 bytes)
		byte[] aesKeyEncrypted=new byte[256];
		is.read(aesKeyEncrypted);
		
		//4. Decrypt with private key
		Cipher cipherForAesKey=getCipherForDecrypt();
		byte[] aesKey=cipherForAesKey.doFinal(aesKeyEncrypted);
		
		//5. Create AES cipher with this key
		SecretKeySpec skeySpec = new SecretKeySpec(aesKey, ALGORITHM);
	    Cipher cipher = Cipher.getInstance(ALGORITHM, "BC");
	    cipher.init(Cipher.DECRYPT_MODE, skeySpec);          	
	    execute(cipher, is, out);
	}


	/**Encrypt a string. Penting diperhatikan bahwa jika str terlalu besar bisa jadi out of memory error
	 * String must be greater than 16 character
	 * @param str
	 * @return str dalam format base64
	 * @throws CipherEngineException
	 */
	public String encryptString(String str) throws CipherEngineException
	{
		str="0123456789abcdef" + str;
		
		ByteArrayInputStream is=new ByteArrayInputStream(str.getBytes());
		ByteArrayOutputStream out=new ByteArrayOutputStream(str.length() * 2);
		try {
			encryptStream(is, out);
		}
		catch(Exception e )
		{
			if(e instanceof NullPointerException)
				throw new CipherEngineException("Cipher is null. Key is not found in folder: /conf");
			else
			throw new CipherEngineException(e);
		} 
		
		String result=Base64.encodeBase64String(out.toByteArray());
		return result;
	}
	
	/**Enkripsi dengan melakukan gzip terlebih dahulu
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws NoSuchProviderException 
	 * @throws InvalidKeyException */
	public void encryptFile(String sourceFile, String resultFile) throws CipherEngineException 
	{
		File file=new File(sourceFile);
		if(file.length() < 16)
			throw new CipherEngineException("File Size must be greater than 16 bytes");

		//prepare output & input
		BufferedOutputStream outFile;
		try {
			outFile = new BufferedOutputStream(new FileOutputStream(resultFile));
			//convert file as ZIP
			File gzipFile= new File(convertFileAsGZIP(sourceFile));
			
			BufferedInputStream inFile=new BufferedInputStream(new FileInputStream(gzipFile));
			
			// 1. Encrypt stream
			encryptStream(inFile, outFile);
	           
	         //2. close stream
	           inFile.close();
	           outFile.close();
              gzipFile.delete();//delete temporary;
		} catch (Exception e) {
			throw new CipherEngineException(e);
		}
	
		
        
	}
	
	public void encryptStream(InputStream inStream,
			OutputStream outStream) throws NoSuchAlgorithmException, NoSuchProviderException, CipherEngineException, IllegalBlockSizeException, BadPaddingException, IOException, InvalidKeyException, NoSuchPaddingException {
		
		//1. Generate AES key
		  KeyGenerator generator = KeyGenerator.getInstance("AES", "BC");
		  generator.init(128);
		  SecretKey secretKey= generator.generateKey();
		  byte[] aesKey=secretKey.getEncoded();

		 //2. encrypt this key with public key  
		  Cipher cipherForAESKey=getCipherForEncrypt();
		  byte[] aesKeyEncrypted=cipherForAESKey.doFinal(aesKey);
		 
		 //3. write version
		  outStream.write(VERSION.getBytes());
		  
		 //4. write aesKeyEncrypted
		  outStream.write(aesKeyEncrypted);
		 
		 //5. write encrypted data 
           SecretKeySpec skeySpec = new SecretKeySpec(aesKey, ALGORITHM);
           Cipher cipher = Cipher.getInstance(ALGORITHM, "BC");
           cipher.init(Cipher.ENCRYPT_MODE, skeySpec);          	
           execute(cipher, inStream, outStream);
		
	}



	private String convertFileAsGZIP(String sourceFile) throws FileNotFoundException, IOException {
		String fileName=sourceFile+ ".gz";
		GZIPOutputStream gzip=new GZIPOutputStream(new FileOutputStream(fileName));
		BufferedInputStream inFile=new BufferedInputStream(new FileInputStream(sourceFile));
		IOUtils.copy(inFile, gzip);
		gzip.close();
		return fileName;
	}



	//Execute cipher
	private void execute(Cipher cipher, InputStream is, OutputStream out) throws IllegalBlockSizeException, BadPaddingException, IOException, CipherEngineException
	{
		if(cipher==null)
			throw new CipherEngineException("Cipher null. private_key dan atau public_key tidak ditemukan");
	
		byte[] data=new byte[1024];
		int count;
		boolean doFinal=true;
		while((count=is.read(data))!=-1)
		{			
			byte[] result=cipher.update(data, 0, count);
			if(result==null)
			{
				result=cipher.update(data, 0, count);
				doFinal=false;
			}
			out.write(result);
		}
		if(doFinal)
			out.write(cipher.doFinal());
	}
	
	/**
	 * Decrypt file
	 * @param sourceFile
	 * @param resultFile
	 * @throws CipherEngineException
	 * @throws IOException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws NoSuchProviderException
	 */
	public void decryptFile(String sourceFile, String resultFile) throws CipherEngineException, IOException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, NoSuchProviderException 
	{		
		//1. Prepare Stream
		String gzipFile=resultFile + ".gz";
		BufferedOutputStream outFile=new BufferedOutputStream(new FileOutputStream(gzipFile));
		BufferedInputStream inFile=new BufferedInputStream(new FileInputStream(sourceFile));
		
		decryptStream(inFile, outFile);
	    
	    //6. gunzip result file
	    inFile.close();
	    outFile.close();

	    gunzipFile(gzipFile, resultFile);
	}

	public void decryptFile(File sourceFile, File resultFile) throws CipherEngineException, IOException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, NoSuchProviderException
	{
		BufferedOutputStream outFile=new BufferedOutputStream(new FileOutputStream(resultFile));
		BufferedInputStream inFile=new BufferedInputStream(new FileInputStream(sourceFile));
		decryptStream(inFile, outFile);
		outFile.close();
		inFile.close();
	}


	private void gunzipFile(String gzipFile, String extractedFile) throws FileNotFoundException, IOException {
		File file=new File(gzipFile);
		GZIPInputStream is=new GZIPInputStream(new FileInputStream(file));
		BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(extractedFile)) ;
		IOUtils.copy(is, out);
		out.close();
		is.close();
		file.delete();//delete gz file
	}



}

package models.jcommon.secure.encrypt;

/**Class ini sebagai 'client' yang melakukan request Web Service ke server
 * 
 * @author andik
 *
 */
public class SecureWSClient extends SecureWS {

	/**Membuat request ke WebService. Response dienkripsi (default)
	 * 
	 * @param url
	 */
	public SecureWSClient(String url) {
		super(url);
	}
	
	/**Jika plainResponse bernilai true maka return dari web service tidak diencripsi.
	 * Pada kasus tertentu, response tidak perlu enkripsi 
	 * @param url
	 * @param plainResponse
	 */
	public SecureWSClient(String url, boolean plainResponse) {
		super(url);
		
	}


}

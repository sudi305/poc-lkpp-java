package models.jcommon.secure.encrypt;

public class CipherEngineException extends Exception {
	public CipherEngineException(Exception msg)
	{
		super(msg);
	}
	
	public CipherEngineException(String msg)
	{
		super(msg);
	}
	
}

package models.jcommon.matrix;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**Class untuk representasi sebuah matrix
 * 
 * @author Mr. Andik
 *
 */
public class Matrix<T> {

	private List<MatrixIndex> columns;
	private List<MatrixIndex> rows;
	
	//internal representation of matrix as Map
	private Map<String, T> data=new HashMap<String, T>();
	
	public Matrix(List<MatrixIndex> columns, List<MatrixIndex> rows)
	{
		this.columns=columns;
		this.rows=rows;
	}
	
	public void setValue(int col, int row, T value)
	{
		data.put(getKey(col, row), value);
	}
	
	public T getValue(int col, int row)
	{
		return data.get(getKey(col, row));
	}
	
	//dapatkan nama-nama kolom
	public List<MatrixIndex> getColumnNames()
	{
		return columns;
	}
	
	public List<MatrixIndex> getRowNames()
	{
		return rows;
	}
	
	private String getKey(int col, int row)
	{
		return col + "." + row;
	}
	
	//convert as CSV
//	public String toCsv()
//	{
////		int colCount=columns.size();
////		int rowCount=rows.size();
////		StringBuilder str=new StringBuilder(colCount * rowCount * 20);
////		for(int row=0; row<rowCount; row++)
////		{
////			str.append('[').append(row).append(" ");
////			for(int col=0; col<colCount; col++)
////			{
////				str.append(str)
////			}
////		}
//	}
}

package models.jcommon.logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;

import play.db.DB;

import com.google.gson.Gson;

/** Pada awalnya data tersimpan di database secara encrypted. Namun hal ini ada kelemahan
 * 1. Karena setiap proses update akan di-log maka jumlah data sangat banyak
 * 2. Untuk itu, log yang sudah lewat satu hari akan dipindah ke file $CONFIG.file-storage-dir/A/log/yyyymmdd.csv.gz
 * 3. Jika kolom 'data' dalam keadaan encrypted maka ketika di-gz hanya 50%;
 *    Jika kolom 'data' tidak encrypted maka di-gz bisa mencapi 90% karena banyak teks yang berulang
 * 4.    
 * @author Mr. Andik
 *
 */
public class ActivityLog extends BaseActivityLog {
	

	
	//get original data
	public String getDecryptedData()
	{
		return decrypt(data);
	}
	
	private static String decrypt(String data)
	{
		byte[] aryOrg=Base64.decodeBase64(data);//original. 
		int len=aryOrg.length-2;	
		byte[] ary2=new byte[len];	//len=8
		int xor1=aryOrg[0];
		int xor2=aryOrg[len+1];
		for(int i=0;i<len;i++)
			ary2[i]=(byte) (aryOrg[i+1] ^ xor1 ^ xor2);
		String str=new String(ary2);
		return str;
	}
	
	public void encryptData() {
		
		byte[] aryByteOriginal=data.getBytes();
		int len=aryByteOriginal.length;
		byte[] aryByte2=new byte[len+2];
		//first & last byte contains xor
		Random rnd=new Random();
		byte xor1=(byte)rnd.nextInt();
		byte xor2=(byte)rnd.nextInt();
		for(int i=0;i<len;i++)
			aryByte2[i+1]=(byte) (aryByteOriginal[i]^xor1^xor2);
		aryByte2[0]=xor1;
		aryByte2[len+1]=xor2;
		data=Base64.encodeBase64String(aryByte2);
		
	}

	

	public static void exportToFile(String userId, String fileName) throws SQLException, IOException {
		
		//parser for action
		Pattern patternAction=Pattern.compile("public static void controllers\\.(.*)\\(.+", Pattern.CASE_INSENSITIVE);
		
		/**
		 * Columns: log_time, user_id, host, data, url, log_id
		 * 
		 * Data: {"action":"public static void controllers.lelang.JadwalCtr.simpan(java.lang.Long,java.util.List,java.lang.Long)","sessionId":"2eff58f3-cb9c-45bc-92ca-ea25353f3329","host":"0:0:0:0:0:0:0:1","logTime":"2014-06-20 11:15:35.86","jadwalList[14]$dtj_tglawal":"28-01-2014 00:00","jadwalList[2]$dtj_id":"12603999","jadwalList[11]$dtj_id":"12612999","jadwalList[12]$dtj_tglawal":"24-08-2014 00:00","jadwalList[7]$aktivitas$akt_id":"123","jadwalList[1]$aktivitas$akt_id":"117","jadwalList[0]$dtj_id":"12574999","jadwalList[11]$aktivitas$akt_id":"127","jadwalList[0]$dtj_tglakhir":"07-07-2014 23:59","jadwalList[1]$dtj_tglakhir":"07-07-2014 23:59","jadwalList[13]$dtj_tglakhir":"27-08-2014 23:59","jadwalList[8]$dtj_id":"12609999","id":"763999","jadwalList[5]$dtj_tglakhir":"09-08-2014 23:59","jadwalList[3]$dtj_tglawal":"01-07-2014 00:00","jadwalList[2]$dtj_tglawal":"01-07-2014 00:00","jadwalList[5]$dtj_tglawal":"08-08-2014 00:00","jadwalList[3]$aktivitas$akt_id":"119","jadwalList[7]$dtj_tglawal":"16-08-2014 00:00","jadwalList[1]$dtj_tglawal":"01-07-2014 00:00","jadwalList[6]$dtj_tglawal":"09-08-2014 00:00","jadwalList[13]$aktivitas$akt_id":"129","jadwalList[9]$aktivitas$akt_id":"125","jadwalList[4]$dtj_tglawal":"08-07-2014 00:00","jadwalList[2]$dtj_tglakhir":"07-07-2014 23:59","jadwalList[14]$dtj_id":"12615999","jadwalList[6]$dtj_tglakhir":"15-08-2014 23:59","jadwalList[5]$dtj_id":"12606999","jadwalList[8]$dtj_tglawal":"17-08-2014 00:00","jadwalList[12]$aktivitas$akt_id":"128","jadwalList[3]$dtj_id":"12604999","jadwalList[8]$aktivitas$akt_id":"124","jadwalList[2]$aktivitas$akt_id":"118","jadwalList[14]$dtj_tglakhir":"29-08-2014 23:59","jadwalList[9]$dtj_tglawal":"18-08-2014 00:00","jadwalList[13]$dtj_tglawal":"26-08-2014 00:00","jadwalList[12]$dtj_id":"12613999","jadwalList[4]$aktivitas$akt_id":"120","jadwalList[8]$dtj_tglakhir":"18-08-2014 23:59","jadwalList[14]$aktivitas$akt_id":"130","jadwalList[1]$dtj_id":"12575999","jadwalList[10]$dtj_tglakhir":"21-08-2014 23:59","jadwalList[10]$dtj_id":"12611999","jadwalList[7]$dtj_tglakhir":"17-08-2014 23:59","jadwalList[7]$dtj_id":"12608999","jadwalList[9]$dtj_id":"12610999","jadwalList[3]$dtj_tglakhir":"07-07-2014 23:59","jadwalList[0]$dtj_tglawal":"01-07-2014 00:00","simpan":"","jadwalList[4]$dtj_tglakhir":"09-07-2014 23:59","jadwalList[10]$aktivitas$akt_id":"126","jadwalList[0]$aktivitas$akt_id":"116","jadwalList[6]$aktivitas$akt_id":"122","jadwalList[11]$dtj_tglakhir":"23-08-2014 23:59","jadwalList[9]$dtj_tglakhir":"19-08-2014 23:59","jadwalList[5]$aktivitas$akt_id":"121","jadwalList[12]$dtj_tglakhir":"25-08-2014 23:59","jadwalList[6]$dtj_id":"12607999","jadwalList[4]$dtj_id":"12605999","jadwalList[11]$dtj_tglawal":"22-08-2014 00:00","jadwalList[10]$dtj_tglawal":"20-08-2014 00:00","jadwalList[13]$dtj_id":"12614999"}
		 * 
		 */
		PrintWriter writer=new PrintWriter(fileName);
		writer.println("<html><body>");
		writer.format("<h2>Activity Log - UserId: %s", userId);
		writer.println("<table><tr><td>[SessionId] [Date Time] [URL]</td><td>Data</td></tr>\n");
		java.sql.PreparedStatement st=DB.getConnection().prepareStatement("select * from logger.activity_log where user_id=? order by log_id asc");
		st.setFetchSize(1000);
		st.setString(1, userId);
		ResultSet rs=st.executeQuery();
		Gson gson=new Gson();
		Map<String, Object> map=new HashMap<String, Object>();
		String sessionId=null;
		while(rs.next())
		{
			String data=decrypt(rs.getString("data"));
			//ada bug saat development; tapi sudah fixed 6agust2014
			if(data.endsWith(",}"))
				data=data.substring(0, data.length()-2) + "}";
			Timestamp date=rs.getTimestamp("log_time");
		//	Logger.debug("[%s] Data: %s", date, data);
			
			map=gson.fromJson(data, map.getClass());
			String action=(String)map.get("action");
			Matcher m=patternAction.matcher(action);
			if(m.matches())
				action=m.group(1);
			if(action.contains(".login"))
			{
				sessionId=(String)map.get("sessionId");
				writer.format("<tr><td colspan=3>[%s] <b>Login at: %s from: %s</b></td></tr>\n",sessionId, date, map.get("host"));
			}
			else
			{
				sessionId=(String)map.get("sessionId");
				{
					writer.format("<tr valign=top><td>[%s]<br>%s<br>%s</td><td>", sessionId, date, action);
					writeMap(writer, map);
					writer.append("</td></tr>\n");
				}
			}
		}
		
		writer.println("</table></body></html>");
		writer.close();
		rs.close();
	}

	private static void writeMap(PrintWriter writer, Map<String, Object> map) {
		Set<Map.Entry<String, Object>> mapEntry=map.entrySet();
		String exludes="null,host,sessionId,logTime,action,";
		for(Map.Entry<String, Object> me:mapEntry)
		{
			String key=me.getKey();
			if(!exludes.contains(key+","))
			{
				writer.print(key);
				writer.print(": ");
				//value bisa berupa String atau ArrayList
				writer.print(me.getValue().toString());
				writer.print("<br>");
			}
		}
		
	}

}

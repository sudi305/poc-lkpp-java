package models.jcommon.util;

/**CLass ini digunakan sebagai flag & notifikasi antara Job dengan WebSocket
 * WS melakukan wait() pada Monitor yang akan di-notify oleh Job
 * @author andik
 *
 */
public class ObjectMonitor
{
	public String status;
	public boolean running;
	public ObjectMonitor()
	{
		status="";
		running=false;
	}
}
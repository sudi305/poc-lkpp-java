package models.jcommon.db.query;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.quirks.QuirksDetector;

import play.Logger;
import play.Play;
import play.jobs.Job;

/**
 * QueryExecutor merupakan class untuk eksekusi SQL yang disimpan di HTML.
 * file HTML disimpan pada folder yang sama dan nama file yg sama dengan class yg extend dari QueryExecutor; dan diakhiri .html
 * setiap SQL berada di dalam satu tag HTML dengan name="sql". Tidak ada ketentuan khusus tentang hal ini.
 * Contoh:
 * 	<div name="lelang-detil">
			TRUNCATE TABLE
				dw.t_dim_lelang_detil</div>
    Di java, SQL didapat dengan 
    	getQuery("lelang-detil");
    atau jika lebih dari 1 element div
    	getQueryList("lelang-detil");
 * 
 * @author Mr. Andik
 *
 */
public abstract class AbstractQueryExecutor {
	private Document doc;
	protected String dbUrl;
	protected String dbPass;
	protected String dbUser;
	private String dbDriver;
		
	/**Catatan
	 * BaseTable.save() akan menggunakan DB connection sama dengan JPA, yaitu yang melekat pada setiap Request.
	 * Untuk Job yang memerlukan durasi eksekusi panjang maka connection akan di-commit setelah job selesai dieksekusi.
	 * Di sini kita menggukan class Instance yang mengeset status saat Job baru saja start dan mengeset status saat job selesai.
	 * Oleh karena itu, code untuk mengeset status TIDAK boleh menggunakan connection dari Job namun harus membuat Job anonim sekedar untuk simpan 
	 * ke DB. 
	 * 
	 * Perlu diingat bahwa Play memiliki pool jumlah job yang running bersamaan. pool ini harus cukup sehingga Job anonim tersebut selalu mendapat
	 * kesempatan untuk running (jadi pool tidak boleh terlalu kecil)
	 * Alternatif lain, untuk menyimpan instance ini harus dengan JDBC langsung tanpa BaseTable
	 * 
	 * @throws IOException
	 */
	public AbstractQueryExecutor() throws IOException
	{
		String name= this.getClass().getName();
		name=name.replace('.', '/')+ ".html";
		File file=Play.getFile("app/"+name);
		doc= Jsoup.parse(file, "UTF-8");
		
		//dapatkan info Koneksi
		dbUrl=getQuery("db.url");
		dbPass=getQuery("db.pass");
		dbUser=getQuery("db.user");
		dbDriver=getQuery("db.driver");
		
		//simpan ini harus di Job baru 
		new Job()
		{
			public void doJob()
			{
//TODO Salah coding, instance harusnya masuk jcommon.
				
//				instance=new Instance(this);
//				instance.start_date=new Date();
//				instance.status=Status.RUNNING;
//				instance.save();
			}
		}.now();
	
	}
	
	public Connection beginTransaction() throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		Class.forName(dbDriver).newInstance();
		Sql2o sql2o=new Sql2o(dbUrl, dbUser, dbPass, QuirksDetector.forURL(dbUrl));
		return sql2o.beginTransaction();
	}
	
	/**Mendapatkan Query dengan nama tertentu, jika di HTML ada banyak 'name' ambil yang pertama
	 * 
	 * @param name
	 * @return
	 */
	public String getQuery(String name)
	{
		Elements els=doc.getElementsByAttributeValue("name", name);
		if(els.size()==0)
			throw new IllegalArgumentException("Element with name='" + name + "' was not found");
		else
			return els.first().text();
	}
	
	/**Mendapatkan Query dengan nama tertentu, lebih dari satu 
	 * @param name
	 * @return
	 */
	public List<String> getQueryList(String name)
	{
		Elements els=doc.getElementsByAttributeValue("name", name);
		if(els==null)
			return null;
		else
		{
			List<String> list=new ArrayList<String>(els.size());
			for(Element el: els)
				list.add(el.text());
			return list;
		}
		
	}
	

	protected abstract void execute();
	

	public static void run(AbstractQueryExecutor executor)
	{
		try
		{
			executor.execute();
		}
		catch(Exception e)
		{
			Logger.error(e, "Error while executing QueryExecutor: %s", executor.getClass().getName());
		}
		
	}

	
	
}

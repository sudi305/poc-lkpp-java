package controllers.jcommon.logger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**Ini merupakan setting untuk logger.  
 * @param optional urlExcludePattern berisi daftar URL yg tidak akan dimasukkan dalam log.
 * @param optional paramExcludePattern berisi daftar paramater (request.params) yang tidak dimasukkan log. Default ini 
 * berisi sbb: paramExcludeDefault="password|password.*|body|authenticityToken|action|controller";
 * Jika diisi maka akan ditambahkan ke nilai default
 * @param methodIncludePattern default adalah POST.
 * @param userIdVariable yaitu nama key pada session untuk userId. Default = 'userId'
 * NOTE: Untuk saat ini, logger belum bisa menangani form yg enctype='multipart/form-data'
 * @author andik
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface LoggerSetting {
	
	public static final String paramExcludeDefault="password|.*password.*|.*passw.*|body|authenticityToken|action|controller";
	
	public String urlExcludePattern() default "";
	public String paramExcludePattern() default paramExcludeDefault;
	public String methodIncludePattern() default "POST";
	public String userIdVariable() default "userId";
}

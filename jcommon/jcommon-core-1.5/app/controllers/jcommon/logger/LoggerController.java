package controllers.jcommon.logger;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import models.jcommon.logger.AbstractLogWriter;
import models.jcommon.logger.ActivityLog;

import org.apache.commons.lang.time.DateUtils;

import play.Logger;
import play.Play;
import play.Play.Mode;
import play.mvc.After;
import play.mvc.Controller;
import play.mvc.Http.Header;

/**This class is an intercepting-Controller.
 * Include in any controller using: <br>
 * <code>
 * @With(controllers.jcommon.logger.LoggerController.class)
 * @LoggerSetting
 * </code>
 * LoggerSetting annotation specify how Logging works. 
 * 
 * @author andik
 *
 */
public class LoggerController extends Controller {

	private static int httpPathLength=0;

	private static boolean warningIsShown=false;
	
	/**Logger dilakukan setelah action.
	 * Kalau ada error pada method maka tidak akan masukk ke dalam logger karena 
	 * 	data tidak akan tersimpan ke database
	 * @throws Exception
	 */
	@After(unless="controllers.jcommon.logger.LoggerController.generateLogger")
	protected static void afterActionInterceptor()
			throws Exception {
		generateLogger();
	}

	/**Mendapatkan alamat remote client dengan mempertimbangkan proxy
	 * Jika ada proxy maka akan didapat: [original_adress, http_proxy_address]
	 * Jika tanpa proxy: original adress
	 * 
	 * @return
	 */
	public static String getRemoteAddress()
	{
		/* Jika melalui proxy, maka ada dapatkan IP address asli. Contoh: 10.1.29.150, 116.66.204.131 
		 * Check using : http://www.ericgiguere.com/tools/http-header-viewer.html		
		 */
		Header header1=request.headers.get("X-Forwarded-For"); //Pastikan header case sensitivity
		if(header1==null)
			header1=request.headers.get("x-forwarded-for");
		
		String host;
		if(header1==null)//jika null artinya langsung tanpa squid_proxy, tanpa apache server
			host=request.remoteAddress;
		else 
			/*Jika tidak null artinya lewat apache server (mod_proxy) dan/atau lewat squid_proxy
			 * request.remoteAddress berisi nama apache server
			 * Contoh: [original_adress, http_proxy_address]
			 */
			host=header1.value();
		return host;
	}
	
	/**
	 * method ini sengaja di-expose barangkali ada action yang perlu logger secara manual 
	 * afterActionInterceptor() tidak bisa dipakai pada logout() karena session sudah di-destroy sehingga tidak ada info userId
	 */
	private static void generateLogger()
	{
		try
		{
			LoggerSetting ann=getControllerInheritedAnnotation(LoggerSetting.class);
			if(ann==null)
			{
				if(!warningIsShown)//tampilkan pesan warning sekali saja dan khusus mode Dev
				{
					if(Play.mode==Mode.DEV)
					{
						String ctr=request.controllerClass.getName();
						Logger.warn("@LoggerSetting is not defined for Controller  '%s' (or its parent). Default will be used: controllers.jcommon.logger.DefaultLoggerSetting", ctr);
					}
					warningIsShown=true;
				}
				//jika Logger setting-nya null maka gunakan default
				ann=new DefaultLoggerSetting();
			}
			
			if(ann.methodIncludePattern()==null)
				return;
			//1. check Method (POST|GET ...)
			String currentMethod=request.method;
			if(!Pattern.matches(ann.methodIncludePattern(), currentMethod))
				return;
			
			//2. check if url included in exclude or not
			String currentURL=request.url;
			
				//remove '${http.path}' from currentURL
			if(httpPathLength==-1)
			{
				String httpPath=Play.configuration.getProperty("http.path");	
				if(httpPath!=null)
					httpPathLength=httpPath.length();
			}
			currentURL=currentURL.substring(httpPathLength);		
			if(ann.urlExcludePattern()!=null)
				if(Pattern.matches(ann.urlExcludePattern(),currentURL)) //found
					return;
			
			String userId=session.get(ann.userIdVariable());
			if(userId==null)
			{
				//coba cari di flash. untuk action Logout(), session sudah destroyed sehingga userId selalu null
				userId=flash.get("userId");
				flash.discard("userId");
				if(userId==null)
					userId="";
			}
		
			String host=getRemoteAddress();
	
			ActivityLog log=new ActivityLog();
			log.user_id=userId;
			log.host=host;
			log.url=request.url;
			long now=System.currentTimeMillis();
			log.log_time=new Timestamp(now);
			log.log_date=DateUtils.truncate(new Date(now), Calendar.DATE);
			StringBuilder paramString=getParamString(log, ann);		
			log.data=paramString.toString();
			AbstractLogWriter.write(log);
		}
		catch(Exception e)
		{
			Logger.error(e.toString());
		}
	}
	
	
	/**Get param as Json.
	 * Sengaja di-compose dari StringBuilder.append(...) agar memiliki high performance. 
	 * @param log */
	private static StringBuilder getParamString(ActivityLog log, LoggerSetting ann) {		
		StringBuilder str=new StringBuilder().append('{');
		
		//tambahkan informasi dari ActityLog
		str.append('"').append("action").append("\":");
		str.append('"').append(request.invokedMethod.toString()).append("\",");
		
		str.append('"').append("sessionId").append("\":");
		str.append('"').append(session.getId()).append("\",");
		
		str.append('"').append("host").append("\":");
		str.append('"').append(log.host).append("\",");
		
		str.append('"').append("logTime").append("\":");
		str.append('"').append(log.log_time.toString()).append("\"");
		
	
		//param yg di-exclude selalu menyertakan default value
		String param=ann.paramExcludePattern() + "|" + LoggerSetting.paramExcludeDefault;
        Pattern pattern = Pattern.compile(param, Pattern.CASE_INSENSITIVE);
		for(String key: params.data.keySet())
		{
			//check if key is excluded or not
			if(!pattern.matcher(key).matches())
			{
		
				str.append(',');
				//mongodb doesn't accept dot, so replace with $
				String key2=key.replace('.', '$');
				appendEncode(str, key2);
				str.append(':');
				String[] values=request.params.getAll(key);
				if(values.length==1)
					//single value
					appendEncode(str, values[0]);
				else
				{	//array
					str.append('[');
					int valCount=values.length;
					for(int i=0;i<valCount;i++)
					{
						if(i>0)
							str.append(',');
						str.append('"');
						str.append(values[i]);
						str.append('"');
					}
					str.append(']');
				}
			}
		}
		str.append('}');
		return str;
	}


	/** 
	 * Append to buff and encode if necessary
	 * @param buff
	 * @param value
	 */
	private static void appendEncode(StringBuilder buff, String value) {
		int len=value.length();
		buff.append('"');
		for(int i=0; i< len; i++)
		{
			char ch=value.charAt(i);
			switch(ch)
			{
				case '"': buff.append("\\\""); break;
				default: buff.append(ch);
			}
		}
		buff.append('"');
	}
	

}

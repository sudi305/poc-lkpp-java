package jobs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.zip.GZIPOutputStream;

import models.jcommon.blob.BlobTableDao;

import org.postgresql.copy.CopyManager;

import play.jobs.Job;

/** DEPRECATED karena log langsung disimpan ke dalam file 
 * Class ini bertujuan untuk melakukan pengarsipan terhadap tabel LOGGER.ACTIVITY_LOG
 * sbb
 * 1. Data yang telah lewat selama 1 hari disimpan ke file 
 * 2. nama file adalah yyyyMMdd.gz
 * 3. Lokasi file di $file-storage-dir/A/log
 * 
 * 
 * @author Mr. Andik
 *
 */
//@On("0 01 00 ? * *") //tiap jam 00:01 tengah malam
@Deprecated
public class ActivityLogArchieverJob extends Job {
	
	private CopyManager cm;
	private Connection conn;
	private PreparedStatement stDelete;
	
	public void doJob()
	{
//		//get start & end EndDate
//		/**Dapatkan tanggal awal & akhir yang akan diarsip.
//		 * Asumsinya, Job ini tidak ada jaminan selalu running pada jam 00:01 sehingga
//		 * ada kemungkinan ada log yang terlewat/tidak diarsip 
//		 * 
//		 */
//		Date today=DateUtils.truncate(new Date(), Calendar.DATE);
//		Object[] dateRange= ActivityLog.find("select min(log_time), max(log_time) from models.jcommon.logger.ActivityLog where log_time<?", today).first();		
//		Timestamp tsStart=(Timestamp) dateRange[0];
//		Timestamp tsEnd=(Timestamp) dateRange[1];
//		
//		Logger.debug("[LogArchiver] MinDate: %s,  MaxDate: %s", tsStart, tsEnd);
//		Date dateEnd=DateUtils.truncate(new Date(tsEnd.getTime()), Calendar.DATE); //dapatkan info tanggal (tanpa waktu)
//		
//		Date dateIterator=new Date(tsStart.getTime());
//		dateIterator=DateUtils.truncate(dateIterator, Calendar.DATE);
//		SimpleDateFormat df=new SimpleDateFormat("yyyyMMdd");
//		
//		dateEnd=DateUtils.addDays(dateEnd, 1);//tambahkan 1 hari agar hari ini masuk dalam hasil
//		try
//		{
//			
//			conn= JdbcUtil.getConnectionWithoutPool();
//			conn.setAutoCommit(true);
//			//get CopyManager of postgres
//			org.postgresql.core.BaseConnection bc=(BaseConnection) conn;
//			stDelete=conn.prepareStatement("DELETE FROM logger.activity_log where log_time< to_date(?, 'YYYYMMDD')");
//			 cm=bc.getCopyAPI();
//			do
//			{				
//				Logger.debug("Processing Date: %s", dateIterator);
//				String dateString=df.format(dateIterator);
//				doArchieve(dateString);//guna	kan 'next' date sebagai parameter
//				dateIterator=DateUtils.addDays(dateIterator, 1);
//			} while(!dateIterator.equals(dateEnd));
//			
//			conn.close();
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
	}

	private void doArchieve(String dateString) throws SQLException, FileNotFoundException, IOException {
		
		File file= new File(BlobTableDao.getFileStorageDir() + "/A/log/" + dateString + ".gz");
		file.getParentFile().mkdirs();
		GZIPOutputStream out=new GZIPOutputStream(new FileOutputStream(file));		
		String sql="COPY (SELECT * FROM logger.activity_log where log_time< to_date('" + dateString + "','YYYYMMDD')) TO STDOUT CSV ";
		cm.copyOut(sql, out);
		//then delete rows already inserted
		stDelete.setString(1, dateString);
		stDelete.executeUpdate();
		out.close();
		//delete 'empty' file; empty .gz is not 0 size
		if(file.length()<100)
			file.delete();
	}
}

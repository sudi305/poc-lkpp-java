package models.dce.db;

import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.codec.binary.Hex;
import org.h2.tools.Csv;
import org.h2.tools.DeleteDbFiles;

import models.jcommon.db.base.JdbcUtil;
import models.jcommon.util.TempFileManager;
import play.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**Exported to H2 database */
public abstract class Exporter {

	
	public final static String MD5_COLUMN_NAME="MD5_ALL_COLUMNS";
	
	public final static String ACTION_UPDATE="@UPDATE";
	
	public final static String ACTION_INSERT="@INSERT";
	
	public final static String ACTION_DELETE="@DELETE";
	
	protected static ComboPooledDataSource dataSourceH2;
	
	protected String sessionId;

	protected String query;

	protected String primaryKey;

	protected String temporaryTableNameMD5;

	protected String primaryKeyString;

	public SqlManipulationSelect manipulator;

	public File temporaryFileMD5;

	public String md5AllRows;
	
	public Exporter(String sessionId, String query, String primaryKey) throws SQLException {
		this.sessionId=sessionId;
		this.primaryKey=primaryKey;
		manipulator=new SqlManipulationSelect(query, primaryKey);
		primaryKeyString=manipulator.primaryKeyString;
		this.query=manipulator.sql;//ada modifikasi query
		init();
	}
	
	public Exporter(String sessionId, String query, String primaryKey,
			boolean containsSubQuery) throws SQLException {
		this.sessionId=sessionId;
		this.primaryKey=primaryKey;
		manipulator=new SqlManipulationSelect(query, containsSubQuery, primaryKey);
		primaryKeyString=manipulator.primaryKeyString;
		this.query=manipulator.sql;//ada modifikasi query
		init();
	}

	protected void init()
	{
		/* nama table perlu dibedakan antara server & client
		 * Penting ketika running di 1 mesin (development) 
		 */
		
		
		if(dataSourceH2==null)
		{
			dataSourceH2=new ComboPooledDataSource();
			//MODE tcp kadang menimbulkan deadlock dg sebab yang tidak diketahui
			//String H2Connection="jdbc:h2:tcp://localhost/" + getH2DatabaseName();
			String H2Connection="jdbc:h2:" + getH2DatabaseName();
			String url=H2Connection;
			String user="sa";
			String password="";
			
			try {				
				dataSourceH2.setDriverClass( "org.h2.Driver" );
				 //loads the jdbc driver
				dataSourceH2.setJdbcUrl(url); 
				dataSourceH2.setUser(user);
				dataSourceH2.setPassword(password); // the settings below are optional -- c3p0 can work with defaults 
				dataSourceH2.setMinPoolSize(5); 
				dataSourceH2.setAcquireIncrement(5); 
				dataSourceH2.setMaxPoolSize(200);
				//drop all objects
				File file=new File(getH2DatabaseName());
				DeleteDbFiles.execute(file.getParent(), file.getName(), true);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
		
	}
	
	
	protected abstract String getH2DatabaseName();

	protected void cleanH2Database() {
		Connection conn;
		try {
			conn = dataSourceH2.getConnection();
			Statement st=conn.createStatement();
			st.executeUpdate("DROP ALL OBJECTS DELETE FILES");
			st.close();
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public abstract void execute() throws SQLException;
	
	public abstract boolean isIncludeAllColumns();
	
	/**Export MD 5 Info dari sebuah Query 
	 * @throws SQLException */
	protected void exportMD5Info() throws SQLException
	{
		
		String sqlMD5=manipulator.getSelectStringMD5(isIncludeAllColumns());
		
		//Logger.debug("MD5 Info, generating using Query: %s", sqlMD5);
		
		if(primaryKey==null)
			throw new IllegalArgumentException("primaryKey cannot be null");
		ResultSet rs =null;

		Connection conn=null;;
		Statement st=null;
	
		try
		{
			conn=JdbcUtil.getConnection();			
			//data may by large, so we must limit fetch size
			st=conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			st.setFetchDirection(ResultSet.FETCH_FORWARD);
		
			//without this, all data is fetch into memory
			st.setFetchSize(1000);
			rs=st.executeQuery(sqlMD5);
		
			
			writeResultsetAsCSV(rs);
			
			if(temporaryFileMD5!=null)
				try {
					importCSV(temporaryFileMD5);
				} catch (SQLException e) {
					try
					{
						e.printStackTrace();
						temporaryFileMD5.delete();
					}
					catch(Exception ee){};
				}
			
		}
		catch(Exception e)
		{
			Logger.error("[Exporter] ERROR Executing Query: %s.\n %s", sqlMD5, e.toString());
			throw new SQLException(e);
		}
		
		try {
			calculateMD5AllRows();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}

	private void calculateMD5AllRows() throws SQLException,
			NoSuchAlgorithmException {
				
		Connection conn=dataSourceH2.getConnection();
		Statement st=conn.createStatement();
		//data may by large, so we must limit fetch size
		st=conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		st.setFetchDirection(ResultSet.FETCH_FORWARD);
	
		//without this, all data is fetch into memory
		st.setFetchSize(1000);
		
		//NOW calculate MD5 for all ROWS
		String sql="SELECT " + MD5_COLUMN_NAME + " FROM " + temporaryTableNameMD5;
		ResultSet rs=st.executeQuery(sql);
		MessageDigest digest=MessageDigest.getInstance("MD5");
		
		while(rs.next())
		{
			String md5EachRow=rs.getString(1);
			digest.update(md5EachRow.getBytes());
			
		}
		rs.close();
		byte[] digestByte=digest.digest();
		md5AllRows=Hex.encodeHexString(digestByte);
		//Logger.debug("MD5 for All %,d ROWS: %s", row, md5AllRows);
				
		st.close();
		conn.close();
	}

	private void importCSV(File file) throws SQLException {
		Connection conn=dataSourceH2.getConnection();
		Statement st=conn.createStatement();
		try
		{
			st.executeUpdate("DROP TABLE IF EXISTS " + temporaryTableNameMD5);
		}
		catch(Exception e) {
			e.printStackTrace();};
		
		st.executeUpdate("CREATE TABLE " + temporaryTableNameMD5 + " AS SELECT * FROM CSVREAD('" + file.toString() + "')");
	
		preparePrimaryKeys(st);
		
		st.close();
		conn.close();
		
	}

	private void preparePrimaryKeys(Statement st) throws SQLException {
		//set primarykey columns as not null
		for(String pkColumn: manipulator.primaryKeys)
		{			
			String sql=String.format("ALTER TABLE %s ALTER COLUMN %s VARCHAR2(1024) NOT NULL", temporaryTableNameMD5, pkColumn);
			st.executeUpdate(sql);
		}
		
		//add primary key columns if exists
		if(manipulator.primaryKeys.length>0)
		{
			String sql=String.format("ALTER TABLE %s ADD CONSTRAINT pk_%s PRIMARY KEY (%s)", temporaryTableNameMD5, temporaryTableNameMD5, primaryKeyString);		
			st.executeUpdate(sql);
		}
	}

	protected void writeResultsetAsCSV(ResultSet rs) throws SQLException, IOException {
		Csv csv=new Csv();
		temporaryFileMD5=TempFileManager.openFile("h2." + temporaryTableNameMD5 + ".csv");
		if(temporaryFileMD5.exists())
			temporaryFileMD5.delete();
		else			
			temporaryFileMD5=TempFileManager.createFileInTemporaryFolder("h2." + temporaryTableNameMD5 + ".csv");
	
		FileWriter writer=new FileWriter(temporaryFileMD5);		
		csv.write(writer, rs);
		writer.close();
		Logger.debug("Temporary CSV File Info: %s, ukuran: %,d KB", temporaryFileMD5.getPath(), temporaryFileMD5.length()/1024);		
	
	}

}

package controllers.dce;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import models.dce.client.ClientExporter;
import models.jcommon.db.exporter.JsonExporter;
import models.jcommon.db.exporter.JsonExporter.FILE_FORMAT;
import models.jcommon.util.TempFileManager;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.apache.commons.io.IOUtils;

import play.Logger;
import play.data.Upload;
import play.mvc.After;
import controllers.jcommon.http.ResumableDownloadController;

/**
 */
public class DCEWebService extends ResumableDownloadController {
	
	public  enum COMMAND
	{
		MD5_TOTAL,
		MD5_PER_ROW;
	}
	
	/**Dapatkan Json dari Sebuah SQL 
	 * @throws NoSuchAlgorithmException */
	@Deprecated
	public static void getJsonFromSQL(String sql) throws SQLException, IOException, CompressorException, NoSuchAlgorithmException
	{
		JsonExporter json=new JsonExporter(FILE_FORMAT.BZIP2);
		File file= json.getJsonAsFile(sql);
		renderBinaryResumable(file, "dce-query.txt");
		file.delete();
	}
	
	/**Pengambilan data dengan Diff
	 * @param action
	 * @param primaryKey
	 * @param sql
	 * @param maxDays
	 * @throws CompressorException 
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws NoSuchAlgorithmException 
	 */
	public static void pullDataWithDiffStep1(String primaryKey, String sql, int maxDays, String data, String requestId) throws IOException, CompressorException, NoSuchAlgorithmException, SQLException
	{
	
		//Logger.debug("pullDataWithDiff(). data: %s, sql: %s, \n\tprimaryKey: %s, requestId: %s", data, sql, primaryKey,  requestId);
		//create JsonExporter
		ClientExporter exporter=new ClientExporter(requestId, sql, primaryKey);
		Object result[]=new Object[2];
		try
		{
			exporter.execute();			
			String md5Total=exporter.md5AllRows;
			boolean isEquals=md5Total.equals(data);
			Logger.debug("[STEP 1 - MD5 Compare] %s, MD5 Server: %s, MD5 client: %s, isEquals: %s", exporter.manipulator.table, data, md5Total, isEquals);
			result[0]="OK";
			result[1]=isEquals;
			
		}
		catch(SQLException  e)
		{
			result[0]="ERROR";
			result[1]="SQL:" + sql + "\nERROR: " + e.toString();
		}
		renderJSON(result);
	}
	
	/**WebService untuk pengambilan data
	 * Output dari WS ini dalam format BZip2
	 * 
	 * @param primaryKey
	 * @param sql
	 * @param maxDays
	 * @param data
	 * @param requestId
	 * @param md5DataUpload
	 * @throws IOException
	 * @throws SQLException
	 */
	public static void pullDataWithDiffStep2(String primaryKey, String sql, int maxDays, String data, String requestId, Upload md5DataUpload) throws IOException, SQLException
	{
		ClientExporter exporter=new ClientExporter(requestId, sql, primaryKey);
		/* jika agregate, server tidak masuk pullDataWithDiffStep1
		 * Sehingga file temporary table belum di-generate => Generate sekarang!
		 */
		if(exporter.manipulator.isAggregate) 
			exporter.execute();
		BZip2CompressorInputStream is=new BZip2CompressorInputStream(md5DataUpload.asStream());
		File file=exporter.mergeMD5DataFromServer(is);
		//result as BZip2
		File tempBzip=compressFile(file);
		renderBinary(tempBzip);
	}
	
	private static File compressFile(File file) throws IOException
	{
		file.deleteOnExit();
		File tempBzip=TempFileManager.createFileInTemporaryFolder(file.getName() + ".bz2");
		BZip2CompressorOutputStream bzip=new BZip2CompressorOutputStream(new FileOutputStream(tempBzip));
		IOUtils.copy(new FileInputStream(file), bzip);
		bzip.close();
		tempBzip.deleteOnExit();
		return tempBzip;
	}
	
	/**WebService untuk menangani fungsi agregate => select sum(...) from ...
	 * 
	 * @param primaryKey
	 * @param sql
	 * @param maxDays
	 * @param data
	 * @param requestId
	 * @param md5DataUpload
	 * @throws IOException
	 * @throws SQLException
	 */
	public static void pullDataWithDiffAggregate(String primaryKey, String sql, int maxDays, String data, String requestId, Upload md5DataUpload) throws IOException, SQLException
	{
		ClientExporter exporter=new ClientExporter(requestId, sql, primaryKey);
		/* jika agregate, server tidak masuk pullDataWithDiffStep1
		 * Sehingga file temporary table belum di-generate => Generate sekarang!
		 *  
		 */
		
		File file=exporter.exportAggregate(sql);
		File fileBzip=compressFile(file);
		renderBinary(fileBzip);
	}

	
}

package controllers;

import play.cache.Cache;
import play.db.jdbc.JdbcPlugin;
import play.mvc.Controller;

public class Application extends Controller {
	public static void index()
	{
		Cache.add("jcommon.workflow.browsing.enable",true);
		render();
	}
}
